
-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'player_round'
-- Player play a round
-- ---

DROP TABLE IF EXISTS `player_round`;
    
CREATE TABLE `player_round` (
  `plyrnd_id` INTEGER NOT NULL AUTO_INCREMENT,
  `Date` DATETIME NOT NULL,
  `round_id` INTEGER NOT NULL,
  `player_id` INTEGER NOT NULL,
  PRIMARY KEY (`plyrnd_id`)
) COMMENT 'Player play a round';

-- ---
-- Table 'player'
-- 
-- ---

DROP TABLE IF EXISTS `player`;
    
CREATE TABLE `player` (
  `player_id` INTEGER NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(40) NOT NULL COMMENT 'md5',
  `globalstats_id` INTEGER NOT NULL,
  PRIMARY KEY (`player_id`)
);

-- ---
-- Table 'round'
-- 
-- ---

DROP TABLE IF EXISTS `round`;
    
CREATE TABLE `round` (
  `round_id` INTEGER NOT NULL AUTO_INCREMENT,
  `perfects` INTEGER NOT NULL DEFAULT 0,
  `greats` INTEGER NOT NULL,
  `goods` INTEGER NOT NULL,
  `bads` INTEGER NOT NULL,
  `miss` INTEGER NOT NULL,
  `style_id` INTEGER NOT NULL DEFAULT 0,
  `score` INTEGER NOT NULL,
  `meter` INTEGER NOT NULL,
  `maxcombo` INTEGER NOT NULL,
  `room_id` INTEGER NOT NULL,
  `song_id` INTEGER NOT NULL,
  PRIMARY KEY (`round_id`)
);

-- ---
-- Table 'room'
-- 
-- ---

DROP TABLE IF EXISTS `room`;
    
CREATE TABLE `room` (
  `room_id` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL DEFAULT 'NoName',
  `description` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`room_id`)
);

-- ---
-- Table 'style'
-- 
-- ---

DROP TABLE IF EXISTS `style`;
    
CREATE TABLE `style` (
  `style_id` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`style_id`)
);

-- ---
-- Table 'global_stats'
-- 
-- ---

DROP TABLE IF EXISTS `global_stats`;
    
CREATE TABLE `global_stats` (
  `globalstats_id` INTEGER NOT NULL,
  `total_perfects` INTEGER NOT NULL,
  PRIMARY KEY (`globalstats_id`)
);

-- ---
-- Table 'song'
-- 
-- ---

DROP TABLE IF EXISTS `song`;
    
CREATE TABLE `song` (
  `song_id` INTEGER NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) NOT NULL,
  `artist` VARCHAR(50) NOT NULL,
  `md5` VARCHAR(40) NOT NULL,
  `link` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`song_id`),
  UNIQUE INDEX(`md5`)
);

-- ---
-- Table 'executable'
-- 
-- ---

DROP TABLE IF EXISTS `executable`;
    
CREATE TABLE `executable` (
  `md5` VARCHAR(32) NOT NULL COMMENT 'md5 exe',
  PRIMARY KEY (`md5`)
);

-- ---
-- Table 'record'
-- 
-- ---

DROP TABLE IF EXISTS `record`;
    
CREATE TABLE `record` (
  `record_id` INTEGER NOT NULL AUTO_INCREMENT,
  `song_id` INTEGER NOT NULL,
  `player_id` INTEGER NOT NULL,
  PRIMARY KEY (`record_id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `player_round` ADD FOREIGN KEY (round_id) REFERENCES `round` (`round_id`);
ALTER TABLE `player_round` ADD FOREIGN KEY (player_id) REFERENCES `player` (`player_id`);
ALTER TABLE `player` ADD FOREIGN KEY (globalstats_id) REFERENCES `global_stats` (`globalstats_id`);
ALTER TABLE `round` ADD FOREIGN KEY (style_id) REFERENCES `style` (`style_id`);
ALTER TABLE `round` ADD FOREIGN KEY (room_id) REFERENCES `room` (`room_id`);
ALTER TABLE `round` ADD FOREIGN KEY (song_id) REFERENCES `song` (`song_id`);
ALTER TABLE `record` ADD FOREIGN KEY (song_id) REFERENCES `song` (`song_id`);
ALTER TABLE `record` ADD FOREIGN KEY (player_id) REFERENCES `player` (`player_id`);

-- ---
-- Test Data
-- ---

-- INSERT INTO `player_round` (`plyrnd_id`,`Date`,`round_id`,`player_id`) VALUES
-- ('','','','');
-- INSERT INTO `player` (`player_id`,`username`,`password`,`globalstats_id`) VALUES
-- ('','','','');
-- INSERT INTO `round` (`round_id`,`perfects`,`greats`,`goods`,`bads`,`miss`,`style_id`,`score`,`meter`,`maxcombo`,`room_id`,`song_id`) VALUES
-- ('','','','','','','','','','','','');
-- INSERT INTO `room` (`room_id`,`name`,`description`) VALUES
-- ('','','');
-- INSERT INTO `style` (`style_id`,`name`) VALUES
-- ('','');
-- INSERT INTO `global_stats` (`globalstats_id`,`total_perfects`) VALUES
-- ('','');
-- INSERT INTO `song` (`song_id`,`title`,`artist`,`md5`,`link`) VALUES
-- ('','','','','');
-- INSERT INTO `executable` (`md5`) VALUES
-- ('');
-- INSERT INTO `record` (`record_id`,`song_id`,`player_id`) VALUES
-- ('','','');


-- ---
-- Table Properties
-- ---

ALTER TABLE `player_round` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `player` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `round` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `room` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `style` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `global_stats` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `song` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `executable` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
ALTER TABLE `record` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `player_round` (`plyrnd_id`,`Date`,`round_id`,`player_id`,`song_id`) VALUES ('','','','','');
INSERT INTO `global_stats` (`globalstats_id`,`total_perfects`) VALUES ('0','0');
INSERT INTO `style` (`style_id`,`name`) VALUES ('0','single');
INSERT INTO `style` (`style_id`,`name`) VALUES ('1','double');
INSERT INTO `executable` (`md5`) VALUES ('e1508f343f6aa5d73f0d36c45c519605');
INSERT INTO `player` (`player_id`,`username`,`password`,`globalstats_id`) VALUES ('0','v1toko','123123','0');
-- INSERT INTO `round_room` (`plyrom_id`,`plyrnd_id`,`room_id`) VALUES ('','','');
-- INSERT INTO `round` (`round_id`,`perfects`,`greats`,`goods`,`bads`,`miss`,`style_id`,`score`,`meter`,`maxcombo`) VALUES ('','','','','','','','','','');
-- INSERT INTO `room` (`room_id`,`name`,`description`) VALUES ('','','');
-- INSERT INTO `record` (`record_id`,`song_id`,`player_id`) VALUES ('','','');

insert into song(artist,title,md5,link) values("SHK","Native","f564e39a7d325859a13ea34449415445","a");

insert into song(artist,title,md5,link) values("Doin","Pavane A.V. Gst 2011 Fiesta EX","9456a16fddb1bab8b577240ed6cd1bc1","a");

insert into song(artist,title,md5,link) values("Unknown Artist","Smells_Like_A_Chocolate","e903883f2904982fefaa534c57098b2c","a");

insert into song(artist,title,md5,link) values("SHK","Take Out","8f295482fa31f3832888daa57e87a51c","a");

insert into song(artist,title,md5,link) values("DM Ashura  *zsm","X-RaVE","8ff58e006a8d3e580cfc5dfcec23ecc2","a");

insert into song(artist,title,md5,link) values("Unknown Artist","Necromancy - Zircon","8e07a0692346699ceff78dc6e52d8b3e","a");

insert into song(artist,title,md5,link) values("Doin","Cleaner","7a938d2d67186b4f1abd2cd13d1e016a","a");

insert into song(artist,title,md5,link) values("Doin","Interference","11f00bd416f838b7c6b24a6fff178823","a");

insert into song(artist,title,md5,link) values("SHK","Reality","16dca139036f3d22d82c81de5c97b4a4","a");

insert into song(artist,title,md5,link) values("Max","Overblow","79134e08a96c8a54f8aa001fb449e9b1","a");

insert into song(artist,title,md5,link) values("MAX","We Got 2 Know","411b541426d084f9ed1072f0ef55ef5f","a");

insert into song(artist,title,md5,link) values("Secret","Magic","f530bed8a1210c438e1de486e00c6cfa","a");

insert into song(artist,title,md5,link) values("4Minute","Hot Issue","0efb417f5e07ecead6633af83aa79e2b","a");

insert into song(artist,title,md5,link) values("Orange Caramel","Magic Girl","ccb56466891965cff2c96a24f772ed92","a");

insert into song(artist,title,md5,link) values("SHINee","Ring Ding Dong","3cd775f6f543923b63cf69537f62e053","a");

insert into song(artist,title,md5,link) values("Beast","Shock","cd6671404c072950630452f52ae97ab9","a");

insert into song(artist,title,md5,link) values("Big Bang","Last Farewell","38f5d7f5927788d26663f173b48d7ae4","a");

insert into song(artist,title,md5,link) values("Norazo","Superman","116fa734e55ab90787c01df7e44e2ac5","a");

insert into song(artist,title,md5,link) values("Mighty Mouth","Energy","4280a6bad6e40590dcded85d1ff06562","a");

insert into song(artist,title,md5,link) values("1TYM","Mother","3e91d3b6e5dbfe8c64146db238f98d36","a");

insert into song(artist,title,md5,link) values("Bae Chi Gi","No.3","b41a48078bb758619fcfc4b2f6df9726","a");

insert into song(artist,title,md5,link) values("Outsider","Like A Man","fc74be8840d5ca1608a831292f9abafc","a");

insert into song(artist,title,md5,link) values("Crash","Crash Day","f6081f4d1cbbea16d1d2edaad2cf43a9","a");

insert into song(artist,title,md5,link) values("Throwdown","What Happened","a0dbe2bba10666d2232af31f1569d179","a");

insert into song(artist,title,md5,link) values("Sanxion7","Gargoyle","f8ee5c4fb28da332a6ce79c55b558a7b","a");

insert into song(artist,title,md5,link) values("DM Ashura","Allegro Con Fuoco","b706c73a1da2acfad3e8d99db6eefa39","a");

insert into song(artist,title,md5,link) values("Banya Production","Hungarian Dance V","6d5aea02a8b2eb23ed6da76e98f8dce1","a");

insert into song(artist,title,md5,link) values("Banya Production","The Devil","f9e7f2ead31e5cb265782f1a1cbc9be3","a");

insert into song(artist,title,md5,link) values("Dm Ashura feat. MC Jay & Veronica","Rave Until The Night Is Over","3df7331077db8be4faa0dd219433b6d0","a");

insert into song(artist,title,md5,link) values("Crash","H00","ef7aa35b32061e8d1710a8031acac69e","a");

insert into song(artist,title,md5,link) values("Mighty Mouth","H00","387c6f599aff28dc754b609fe8d9772b","a");

insert into song(artist,title,md5,link) values("Outsider","H00","520a7223175de97cc437c0330a990e30","a");

insert into song(artist,title,md5,link) values("Orange Caramel","H00","c179f9a9a6508773b70f65016e386d44","a");

insert into song(artist,title,md5,link) values("Beast","H00","22076c118d0d1a2c446eab39c57ab720","a");

insert into song(artist,title,md5,link) values("Wax","I'll give you all my love","b96b0723b9380dcf440b96a6f0588b47","a");

insert into song(artist,title,md5,link) values("Duke","Starian","2ae896a48fbfc85136848eed15658d42","a");

insert into song(artist,title,md5,link) values("Eun Ji Won","Adios","4a22735d5b77e2cb107c447e74aaa8d1","a");

insert into song(artist,title,md5,link) values("Banya","Beethoven Virus","b9202e0703bbbed9ac2dc2427405f4bc","a");

insert into song(artist,title,md5,link) values("Banya","Canon-D","3d87158afada3f1e1e1757894d4b026f","a");

insert into song(artist,title,md5,link) values("BanYa Production","Toccata","183b9af8e0c061b206464467ab83f4f2","a");

insert into song(artist,title,md5,link) values("Duke","Starian","f782476dea3b08e157086649039b9aba","a");

insert into song(artist,title,md5,link) values("Tico","Pump Breakers","2fa489b51725a2a0f78b3e7832713d5c","a");

insert into song(artist,title,md5,link) values("Yahpp","Cannon X.1","77be2a53f877a654c35ebe6f3d937150","a");

insert into song(artist,title,md5,link) values("Big Bang","LaLaLa","6594bba881d8c69002461d89248d171a","a");

insert into song(artist,title,md5,link) values("Tashannie","Don't bother me","762cdff76e09373e5c37259ba4e0ebe9","a");

insert into song(artist,title,md5,link) values("Hoobastank","Out of control","e5bfcd0d72e2eaa901698717a30e3faf","a");

insert into song(artist,title,md5,link) values("Unknown artist","Toya","71758f2e9146434221995ea9eb67ddba","a");

insert into song(artist,title,md5,link) values("Unknown artist","Yamajet","d541eaff7f0276dac8d6e2bdd688374b","a");

insert into song(artist,title,md5,link) values("Dj Doc","One Night","bdc3fe886af534732b3c068b4c42a57a","a");

insert into song(artist,title,md5,link) values("45rpm","Slightly","7cd03894f2075be01da231c86b9c49fd","a");

insert into song(artist,title,md5,link) values("May","Compuction","c5bde00cd5677e4172453fccbe9868ba","a");

insert into song(artist,title,md5,link) values("Hot Potato","No Despair","4d95d9a7946a381471f2ed027020b2ed","a");

insert into song(artist,title,md5,link) values("Unknown artist","streched","4d2986c314794fc184a389bc93e366da","a");

insert into song(artist,title,md5,link) values("Unknown Artist","00 Remix Wi Be Ex Cle Doc Vacc Mera","36a7fdf9fc99d70d2391a1ee40fb8a67","a");

insert into song(artist,title,md5,link) values("Yahpp","Faster Z","23fdea1a6ff884e02358bbcd10320d5d","a");

insert into song(artist,title,md5,link) values("2NE1","Fire","45d0035b5a73b123440e6dffb560a2c7","a");

insert into song(artist,title,md5,link) values("Crash","Lucid Sycophant","480e05fcef196c08a95629bb44cd3454","a");

insert into song(artist,title,md5,link) values("Apocalyptica","Delusion Full Song","4a2d24957e0cbbd1c67c46fd7cc76aef","a");

insert into song(artist,title,md5,link) values("Unknown Artist","06.ASDF","bcd38cb662590a0863cf0e530ba4e808","a");

insert into song(artist,title,md5,link) values("Dj Sharpnel","Blue Army","9ad13138b2d6f34319d20be9a67cf8a5","a");

insert into song(artist,title,md5,link) values("Cyrex","D011","5cbd5404accc53f658fea247c70e8459","a");

insert into song(artist,title,md5,link) values("Dark Moor","Devil In The Tower","dbaabc59b0ce6f9c369574de336137a0","a");

insert into song(artist,title,md5,link) values("Dark Moor - SM By Criss","Dies Irae Full Song","fe7eb908f50c5b08ba48a3277fdd903e","a");

insert into song(artist,title,md5,link) values("Disturbed","Indestructible","5c1ec76decb0bfc9c541941117eac082","a");

insert into song(artist,title,md5,link) values("Doin","Night Duty","301ef1526d0772bf562cc3295031cf5e","a");

insert into song(artist,title,md5,link) values("Unknown artist","Final Downfall","438dfa034cd852a999b006b16d4e920c","a");

insert into song(artist,title,md5,link) values("2NE1","Fire","1de838a2587258f4e5149687cd9c935f","a");

insert into song(artist,title,md5,link) values("BanYa Production","Get up & Go","03e195f0491baa8cacb44789568c9fff","a");

insert into song(artist,title,md5,link) values("msgoon","msgoon RMX pt.6","7cffb2bfc7b8c825fac1bf45d8e4f37d","a");

insert into song(artist,title,md5,link) values("msgoon","msgoon RMX pt.7","d00414e79992f0b71d398670309d5728","a");

insert into song(artist,title,md5,link) values("crash and vassline","hard core rock remix","8bdb2d47d29a090111313a2c7226c4bd","a");

insert into song(artist,title,md5,link) values("Kinato","T.S","860d04bb47e4e8bfd613b0b1edff5d82","a");

insert into song(artist,title,md5,link) values("Unknown Artist","Makou","a10b8df730999f09963f2b3f0896ad6d","a");

insert into song(artist,title,md5,link) values("Unknown Artist","Mayhem","0387fdc1e06919b730c799dedda74477","a");

insert into song(artist,title,md5,link) values("Kommisar","Mecha-Tribe Assault","cd7c9d3e0e1c4c1f5ab11ed3fea6d798","a");

insert into song(artist,title,md5,link) values("Unknown artist","meduzz","5e8f37ff81b500c01e61e76112c95edd","a");

insert into song(artist,title,md5,link) values("Banya","Night Duty","ef9ac5299ea9d38017f737decde0e756","a");

insert into song(artist,title,md5,link) values("Unknown artist","Orange Caramel","255ba963163fee6eaf93c445c7f20732","a");

insert into song(artist,title,md5,link) values("Outsider","Alone","0b0cea99e580c1184cc0021f33870263","a");

insert into song(artist,title,md5,link) values("Doin","Pavane A.V. Gst 2011 Fiesta EX","9834dd0973a83e83918f0e6fb12a49cc","a");

insert into song(artist,title,md5,link) values("DJ CYO","Take Destinatiomancy Remix","e14aad1c751969a1a264fb9b9b815c17","a");

insert into song(artist,title,md5,link) values("Bethoven","Sonata claro de luna","2c57826707264c071573b0fcc50891b3","a");

insert into song(artist,title,md5,link) values("Kommisar","Springtime","59f42d9aa49d99bbfa4c8189a06cfa5f","a");

insert into song(artist,title,md5,link) values("Doin","Vacuum Cleaner REMIX","42ca47c17733c4a4d24da1cd269c388e","a");

insert into song(artist,title,md5,link) values("Unknown Artist","Necromancy - Zircon","e6df108554c801aa9ecdc209fda159a7","a");


