package cl.stepnxa.server;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

/**
 *
 * @author Victor
 */
public final class PacketUtils {
    
    private static final int SIZEOF_BYTE = 1;
    private static final int SIZEOF_SHORT = 2;
    private static final int SIZEOF_INT = 4;
    private static final int SIZEOF_LONG = 8;

    private static final byte SERVER_VERSION = (byte)255;

    //server commands
    private static final byte CMD_PING = (byte)128;
    private static final byte CMD_HELLO = (byte)130;
    private static final byte CMD_ALLOWSTART = (byte)131;
    private static final byte CMD_GAMEOVERSTATS = (byte)132;
    private static final byte CMD_SCOREBOARDUPDATE = (byte)133;
    private static final byte CMD_SYSTEMMESSAGE = (byte)134;
    private static final byte CMD_CHAT = (byte)135;
    private static final byte CMD_STARTSELECTSONG = (byte)136;
    private static final byte CMD_UPDATEUSERLIST = (byte)137;
    private static final byte CMD_FORCESCREEN = (byte)138;
    private static final byte CMD_RESERVED = (byte)139;
    private static final byte CMD_SMOPACKET = (byte)140;
    private static final byte CMD_UDPPACKET = (byte)141;
    private static final byte CMD_ATTACKCLIENT = (byte)142;
    private static final byte CMD_XMLREPLY = (byte)143;
    private static final byte CMD_RANKEDSONG = (byte)144;
    private static final byte CMD_STYLEINFO = (byte)146;
    //extra commands
    private static final byte CMD_SMO_LOGIN = (byte)0;
    private static final byte CMD_SMO_UPDATEROOMS = (byte)1;

    public static byte[] UpdateUserList(Vector<Client> c)
    {
        //pkt size + cmd + max # players + # of players + player status * size() + player name
        int i = 0;
        for( Client s : c)
            i += s.m_sUsername.length()+1; //+1 por
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + (SIZEOF_BYTE*c.size()) + i;
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_UPDATEUSERLIST);
        buf.put((byte)32);
        buf.put((byte)c.size());

        for(Client s : c)
        {
            buf.put((byte)s.m_iState);
            buf.put(s.m_sUsername.concat("\0").getBytes());
        }

        return buf.array();
    }

    public static byte[] UpdateScoreBoard(Vector<Client> c, int iSection)
    {
        ByteBuffer buf = null;
        switch( iSection )
        {
            case 0:
            {
                Comparator<Object> comp = new PlayerComparator();
                Object[] a = c.toArray();
                Arrays.sort(a, comp);
                
                //pkt size + cmd + section to send + #players + index
                //nuevo sistema #index se cambia por playerName
                int lenNames = 0;
                for( Client s : c)
                    lenNames += s.m_sUsername.length()+1; //+1 por
                int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + lenNames;
                buf = ByteBuffer.allocate(iCapacity);
                buf.putInt(iCapacity-SIZEOF_INT);
                buf.put(CMD_SCOREBOARDUPDATE);
                buf.put((byte)iSection);
                buf.put((byte)c.size());

                c.clear();
                for(int i=0; i < a.length; i++ )
                    c.add((Client)a[i]);
                
                for(Client s : c)
                    buf.put(s.m_sUsername.concat("\0").getBytes());
            }
            break;
            case 1:
            {
                //pkt size + cmd + section to send + #players + score
                int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + (SIZEOF_INT*c.size());
                buf = ByteBuffer.allocate(iCapacity);
                buf.putInt(iCapacity-SIZEOF_INT);
                buf.put(CMD_SCOREBOARDUPDATE);
                buf.put((byte)iSection);
                buf.put((byte)c.size());

                for( int i = 0; i < c.size(); i++)
                    buf.putInt(c.get(i).GetScore());
            }
            break;
            case 2:
            {
                //pkt size + cmd + section to send + #players + grade
                int iCapacity = SIZEOF_INT + SIZEOF_BYTE + SIZEOF_BYTE + SIZEOF_BYTE + (SIZEOF_BYTE*c.size());
                buf = ByteBuffer.allocate(iCapacity);
                buf.putInt(iCapacity-SIZEOF_INT);
                buf.put(CMD_SCOREBOARDUPDATE);
                buf.put((byte)iSection);
                buf.put((byte)c.size());

                for( int i = 0; i < c.size(); i++)
                    buf.put((byte)c.get(i).GetGrade());
            }
            break;
        }

        return buf.array();
    }

    public static byte[] AllowStart()
    {
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE;
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_ALLOWSTART);

        return buf.array();
    }

    public static byte[] LoginResponse(String sResponse, int approval)
    {
        sResponse = sResponse.concat("\0");
        //packet size + command + smocmd + aproval status + login response length
        int iCapacity = SIZEOF_INT + (SIZEOF_BYTE * 3) + sResponse.length();

        ByteBuffer buf = ByteBuffer.allocate(iCapacity);
        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_LOGIN);
        buf.put((byte)approval);
        buf.put(sResponse.getBytes());

        return buf.array();
    }

    public static byte[] UpdateAllRoms()
    {
        Vector<Room> r = (Vector<Room>) GameManager.getInstance().getRooms().clone();
        if( r.remove(GameManager.getInstance().GetLobby()) )
            System.out.println("Lobby removed from update");

        System.out.println("Prepared for update: # " +r.size() + " rooms");
        
        int iAllRoomsSize = 0;
        for( Room s: r )        
            iAllRoomsSize += (s.m_sTitle.length()+1) + (s.m_sDescription.length()+1);

        //packet size + command + smocommand + # of rooms + length of all roms name and desc + # of room by state and flags
        int iCapacity = SIZEOF_INT + 
                (2*SIZEOF_BYTE) +
                (SIZEOF_BYTE*2) + //update list or room of game and # of rooms
                iAllRoomsSize +
                (SIZEOF_BYTE*r.size())*2;
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SMOPACKET);
        buf.put(CMD_SMO_UPDATEROOMS);
        buf.put((byte)1); //update list of rooms
        buf.put((byte)r.size());
        //room names and desc
        for( Room s: r )
        {
            buf.put(s.m_sTitle.concat("\0").getBytes());
            buf.put(s.m_sDescription.concat("\0").getBytes());
        }
        //status
        for( Room s: r )
            buf.put((byte)s.m_iStatus);
        //passworded?
        for( Room s: r )
            buf.put(s.m_sPassword.isEmpty() ? (byte)0 : (byte)1);

        return buf.array();
    }

    public static byte[] ChatMessage(String sMessage)
    {
        sMessage = sMessage.concat("\0");
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + sMessage.length();
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_CHAT);
        buf.put(sMessage.getBytes());

        return buf.array();
    }

    public static byte[] SystemMessage(String sMessage)
    {
        sMessage = sMessage.concat("\0");
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE + sMessage.length();
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT);
        buf.put(CMD_SYSTEMMESSAGE);
        buf.put(sMessage.getBytes());

        return buf.array();
    }

    public static byte[] Hello(String sServerName)
    {
        sServerName = sServerName.concat("\0");
        //packet size and info
        //string.length() + 1 for Null terminate
        int iCapacity = SIZEOF_INT + (2 * SIZEOF_BYTE) + (sServerName.length());
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(iCapacity-SIZEOF_INT); //restamos el packet size
        buf.put(CMD_HELLO);
        buf.put(SERVER_VERSION);
        buf.put(sServerName.getBytes());

        return buf.array();
    }

    public static byte[] Ping()
    {
        //packet size and info
        int iCapacity = SIZEOF_INT + SIZEOF_BYTE;
        ByteBuffer buf = ByteBuffer.allocate(iCapacity);

        buf.putInt(SIZEOF_BYTE);
        buf.put(CMD_PING);

        return buf.array();
    }
    
    /*
     * Clase utilitaria para ordenar los clientes por score
     */
    private static class PlayerComparator implements Comparator<Object>
    {
        public int compare(Object t, Object t1) {
            Client c = (Client)t;
            Client c1 = (Client)t1;
            if( c.GetScore() > c1.GetScore())
                return -1;
            else if( c.GetScore() == c1.GetScore() )
                return 0;
            else if( c.GetScore() < c1.GetScore() )
                return 1;
            
            return 0;
        }        
    }

}
