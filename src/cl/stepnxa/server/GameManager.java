package cl.stepnxa.server;

import java.util.Vector;

/**
 * Clase administradora del juego
 * @version 0.1, 12-04-2012
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class GameManager {
    private static GameManager INSTANCE = null;
    private Vector<Client> m_vClients = null;
    private Vector<Room> m_vRooms = null;

    /**
     * Agrega al server una room
     * @param r Sala a agregar al server
     */
    public void AddRoom(Room r)
    {
        if( getRooms() == null )
            m_vRooms = new Vector<Room>();

        getRooms().add(r);
        UpdateRoomList();
    }

    /**
     * Quitamos una sala del server
     * @param r Sala a eliminar
     */
    public void DeleteRoom(Room r)
    {
        if( getRooms() == null )
        {
            m_vRooms = new Vector<Room>();
            //nada que borrar
            return;
        }
        
        if( getRooms().remove(r) )
            System.out.println("Deleted room: " + r.m_sTitle);
        else
            System.out.println("Room not deleted");
        UpdateRoomList();
    }

    /**
     * Agrega un cliente al lobby
     * @param c Cliente a agregar al lobby
     */
    public void AddClientToLobby(Client c)
    {
        Room r = GetLobby();
        r.AddClient(c);
    }

    /**
     * Elimina un cliente del lobby
     * @param c Cliente a quitar del lobby
     */
    public void RemoveClientFromLobby(Client c)
    {
        Room r = GetLobby();
        r.RemoveClient(c);
    }

    /**
     * Envia a los cliente (dentro del lobby) las salas activas
     */
    public void UpdateRoomList()
    {
        if( getRooms() == null || getClients() == null )
        {
            System.out.println("Nulled rooms");
            return;
        }

        if( getRooms().isEmpty() /*|| getClients().isEmpty()*/ )
        {
            System.out.println("Empty rooms");
            return;
        }        

        for( int i = 0; i<getClients().size() ; i++)
        {
            if(getRooms().size() > 1)//lobby siempre está
            {
                //enviamos solo a los clientes conectados
                if(getClients().get(i).m_bConnected)
                {
                    System.out.println("Updated rooms: " + getRooms().size());
                    getClients().get(i).UpdateRoomList(getRooms());
                }
            }
        }
    }

    /**
     * Quitamos un cliente de una sala
     * @param c es el cliente a eliminar
     * @return La room en la que quitamos el cliente
     */
    public Room RemoveClientFromRoom(Room r, Client c)
    {
        r.RemoveClient(c);
        if( r.IsEmpty() && !r.IsLobby() )
        {
            DeleteRoom(r);
            return null;
        }
        return r;
    }

    /**
     * Quitamos un cliente de una sala
     * @param sRoomName Nombre de la sala que tiene al cliente
     * @param c es el cliente a eliminar
     */
    public void RemoveClientFromRoom(String sRoomName, Client c)
    {
        //
        //  TEST THIS!
        //
        Room r = this.GetRoomByName(sRoomName);
        RemoveClientFromRoom(r, c);
    }

    /**
     * Agregamos un cliente a una sala
     * @param r La sala a la que hay que agrega al cliente
     * @param c El cliente a agrega
     */
    public void AddClientToRoom(Room r, Client c)
    {
        r.AddClient(c);
    }

    /**
     * Agregamos un cliente al server
     * @param c es el cliente a agregar
     */
    public void AddClient(Client c)
    {
        if( getClients() == null )
            m_vClients = new Vector<Client>();

        getClients().add(c);
    }

    /**
     * Quitamos un cliente del server
     * @param c es el cliente a eliminar
     */
    public void RemoveClient(Client c)
    {
        //lo quitamos de la sala y luego del server.
        if( getClients().isEmpty() )
            return;

        /*if( c.m_Room != null ) //si está en una room, lo quitamos de ahi tb
        {            
            //
            //nullpointer exception
            c.m_Room.RemoveClient(c);
        }
        else //lobby
        {
            Room r = this.GetRoomByName("Lobby");
            r.RemoveClient(c);
        }
        * 
        */
        getClients().remove(c);
    }

    /**
     * Obtenemos una sala por su nombre
     * @param sName Nombre de la sala
     * @return La sala
     */
    public Room GetRoomByName(String sName)
    {
        for( int i = 0; i < getRooms().size(); i++)
        {
            if( getRooms().get(i).m_sTitle.compareTo(sName) == 0 ) //igual sin ignorar mayusculas
            {
                return getRooms().get(i);
            }
        }
        return null;
    }

    /**
     * Obtenemos el lobby
     * @return El lobby (sala)
     */
    public Room GetLobby()
    {
        Room r = this.GetRoomByName("Lobby");
        return r; 
    }

    // Private constructor suppresses
    private GameManager() {}

    // creador sincronizado para protegerse de posibles problemas  multi-hilo
    // otra prueba para evitar instanciación múltiple
    private synchronized static void createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new GameManager();
        }
    }

    /**
     * Obtenemos la instancia del singleton
     * @return La instancia de la clase GameManager
     */
    public static GameManager getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    /**
     * Devuelve los clientes del server
     * @return Todos los clientes del server
     */
    public Vector<Client> getClients() {
        return m_vClients;
    }

    /**
     * Devuelve las salas del server
     * @return Todos las salas del server
     */
    public Vector<Room> getRooms() {
        return m_vRooms;
    }
}
