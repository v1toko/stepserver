package cl.stepnxa.server;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Clase de entrada al programa
 * @version 0.1, 12-04-2012
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Server {

    //puerto en el que se bindea el socket
    static final int PUERTO = 8765;

    /**
     * Inicia el programa y los threads, también deja a la escucha el socket y agrega los clientes     
     */
    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(PUERTO);
            //Cuando entramos al servidor los clientes están en el lobby
            //creamos el lobby y cuando un cliente se conecta, lo ingresamos ahi
            Room lobby = new Room(0, "Lobby", "The Main Lobby", "", true);
            GameManager.getInstance().AddRoom(lobby);

            //
            // TODO: synchronize threads 
            /*
            Thread pinger = new Thread(new Runnable() {

                public void run() {
                    while (true) {                        
                        java.util.ArrayList<Client> c = GameManager.getInstance().getClients();                        
                        if (c != null) {                            
                            //System.out.println("Pinging #Clients: " + c.size());
                            for (int i = 0; i < c.size(); i++) {
                                if(c.get(i).m_bConnected)
                                    c.get(i).PingClient();
                            }
                        }

                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, "Pinger");
            pinger.start();   
            * */
            

            while (true) {
                System.out.println("Listening for connections on port 8765... ");
                Socket client = serverSocket.accept();
                client.setSoTimeout(2000);//ping time
                Thread t = new Thread(new Client(client));
                t.start();
                System.out.println("Connected - " + client.getInetAddress());
                Thread.sleep(20);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Método de entrada al programa.
     * @param args Argumentos de entrada al programa
     */
    public static void main(String[] args) {
        Server server = new Server();
        server.go();
    }

    /**
     * Crea un objeto Server
     */
    public Server() {

        /*
        Server GameServer = new Server(PUERTO);
        boolean bServing = true;
        if (!GameServer.CreateSocket()) {
        bServing = false;
        }

        //el socket se creo correctamente
        //ahora podemos continuar "sirviendo"
        if (bServing) {
        System.out.println("Socket Creado correctamente.");
        Thread t = new Thread(GameServer);
        t.start();
        do {
        t.yield();
        for (int i = 0; i < GameServer.getClients().size(); i++) {
        Logger.Trace("Updating Client");
        GameServer.getClients().get(i).Update();
        if (GameServer.getClients().get(i).m_Connection.isClosed()) {
        GameServer.getClients().remove(i);
        }
        }
        //GameServer.CheckClient();

        try {
        Thread.sleep(1000);
        } catch (Exception e) {
        Logger.Trace(toString() + ":: Error on Server()::Sleep");
        }


        } while (true);
        }

        if (GameServer.CloseSocket()) {
        System.out.println("Server():: Cerrando servidor.");
        }
         */

        /*
        try {
        ServerSocket skServidor = new ServerSocket(PUERTO);
        System.out.println("Escucho el puerto " + PUERTO);
        boolean quit = false;
        for (;!quit;)
        {
        System.out.println("Socket Listo...");
        Socket skCliente = skServidor.accept(); // Crea objeto
        //System.out.println("Sirvo al cliente " + numCli);
        System.out.println("Cliente aceptado");
        OutputStream aux = skCliente.getOutputStream();
        DataOutputStream flujo = new DataOutputStream(aux);

        InputStream input = skCliente.getInputStream();
        DataInputStream in = new DataInputStream(input);

        //flujo.writeUTF("Hola cliente " + numCli);

        //flujo desde el cliente
        in.skipBytes(4);
        byte []b = new byte[1024];
        int iReads = in.read(b);
        System.out.println("Bytes Readed: " + iReads);
        String str = new String(b);
        System.out.println(str);

        //Escribimos los bytes uno por uno para mandar al cliente
        flujo.writeByte(0);
        flujo.writeByte(0);
        flujo.writeByte(0);
        flujo.writeByte(11);
        flujo.writeByte(130);
        flujo.writeByte(129);
        //flujo.writeByte(64);
        //flujo.write(new String("StepNXA1F").getBytes());
        flujo.writeBytes("StepNXA\0");
        flujo.writeByte(8237489);

        //skCliente.close(); //cerrar el socket y desconectar al cliente
        Scanner s = new Scanner(System.in);
        if( s.hasNext() )
        quit = true;
        }
        System.out.println("Demasiados clientes por hoy");
        } catch (Exception e) {
        System.out.println(e.getMessage());
        }
         */
    }
}
