package cl.stepnxa.server;

/**
 * Clase que guarda la información de la ultima canción seleccionada
 * @version 0.1, 12-04-2012
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Song {
    //Hash identificador de la canción
    private String m_sHash;
    //Titulo de la canción
    private String m_sTitle;
    //Artista de la canción
    private String m_sArtist;
    //Si existe la cancion en el servidor
    private boolean m_bExist = false;
    //id
    private int m_iId = 0;

    /**
     * Crea un objeto Song
     * @param title Titulo de la canción
     * @param artist Artista de la canción
     * @param hash Hash identificador de la canción
     */
    public Song(String title, String artist, String hash, int id)
    {
        m_sTitle = title;
        m_sArtist = artist;
        m_sHash = hash;
        //lo seteamo una sola vez.
        m_bExist = ExistInServer();
        m_iId = id;
    }

    /**
     * Crea un objeto Song
     */
    public Song()
    {
        m_sArtist = "";
        m_sHash = "";
        m_sTitle = "";
        m_iId = 0;
    }
    
    public boolean getExistInServer()
    {
        return m_bExist;
    }

    /**
     * Devuelve si la canción existe en el server
     * @return  true si la canción está en la BD
     */
    private boolean ExistInServer()
    {
        return MySQLManager.getInstance().isRankedSong(m_sHash) != null;
    }

    /**
     * Devuelve el hash de la canción
     * @return El Hash identificador de la canción
     */
    public String getHash() {
        return m_sHash;
    }

    /**
     * Cambia el hash de la canción
     * @param m_sHash El hash para cambiar
     */
    public void setHash(String m_sHash) {
        this.m_sHash = m_sHash;
    }

    /**
     * Devuelve el titulo de la canción
     * @return El titulo de la canción
     */
    public String getTitle() {
        return m_sTitle;
    }

    /**
     * Cambia el titulo de la canción
     * @param m_sTitle El titulo para cambiar
     */
    public void setTitle(String m_sTitle) {
        this.m_sTitle = m_sTitle;
    }

    /**
     * Devuelve el artista de la canción
     * @return El artista de la canción
     */
    public String getArtist() {
        return m_sArtist;
    }

    /**
     * Cambia el artista de la canción
     * @param m_sArtist El artista para cambiar
     */
    public void setArtist(String m_sArtist) {
        this.m_sArtist = m_sArtist;
    }

}
