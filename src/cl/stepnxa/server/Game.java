package cl.stepnxa.server;

import cl.stepnxa.persistence.PlayerRound;
import cl.stepnxa.persistence.Record;
import cl.stepnxa.persistence.Round;
import java.util.Vector;
import cl.stepnxa.persistence.Song;
import java.util.Date;
import javax.persistence.*;

/**
 * Mantiene la información del juego en curso
 *
 * @version 0.1, 13-04-2012
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Game {

    public enum GameState {

        NONE,
        Playing, //ingame
        FirstSelect, //first song select
        SecondSelect //confirm song select
    }
    //estado del juego
    private GameState m_GameState = GameState.NONE;
    //clientes en el juego
    private Vector<Client> m_vClients = new Vector<Client>();
    //Song que se jugará
    private Song m_Song = new Song();

    /**
     * Constructor por defecto de la clase Game
     */
    public Game() {
    }

    public void StartGame(Song song) {
        switch (m_GameState) {
            case NONE:
            case FirstSelect: {
                SelectSong(song);
            }
            break;
            case SecondSelect: {
                //and hash
                if (m_Song.getTitle().compareTo(song.getTitle()) == 0
                        && m_Song.getArtist().compareTo(song.getArtist()) == 0) {
                    //lackers
                    for (Client c : getClients()) {
                        if (c.m_bLacker) {
                            for (Client f : getClients()) {
                                f.SendServerChat("No se puede iniciar la ronda, no todos los players tiene la cancion");
                            }
                            return;
                        }
                    }
                    //si todos los players tienen la cancion, estamos bien.
                    for (Client c : getClients()) {
                        c.SendServerChat("Starting song");
                    }
                    this.m_vClients.get(0).m_Room.m_iStatus = 2;
                    GameManager.getInstance().UpdateRoomList();
                    StartGame();
                    //System.out.println("Starting song");
                } else {
                    SelectSong(song);
                }
            }
            break;
            case Playing:
                break;
        }
    }

    /**
     * Quita un cliente del juego
     *
     * @param c Cliente del juego
     */
    public void RemoveClient(Client c) {
        if (getClients() == null) {
            m_vClients = new Vector<Client>();
        }

        getClients().remove(c);
        //if (!getClients().isEmpty()) {
        //System.out.println("Removing client from game: " + c.m_sUsername);            
        //}
    }

    /**
     * Envia los scores finales de todos los players
     */
    public void HandleGameOver(Room room) {
        //send the stats only if all players informed their roundstate
        //Vector<Client> alc = (Vector<Client>) getClients().clone();
        boolean bAllReady = true;
        for (Client c : getClients()) {
            if (c.GetRoundState() != Client.RoundState.State_Ended) {
                bAllReady = false;
            }
        }

        //solo si todos los players están terminados, entonces mandamos las gameover stats
        if (bAllReady) {
            for (Client c : getClients()) {
                //si se está conectado, sino "gud bai"
                if (c.m_bConnected) {

                    if (room.m_Song.getRanked()) {
                        Round r = new Round();
                        r.setRoundId(0);
                        r.setMaxcombo(c.GetMaxCombo());
                        r.setMiss(c.GetMisses());
                        r.setBads(c.GetBads());
                        r.setGoods(c.GetGoods());
                        r.setGreats(c.GetGreats());
                        r.setPerfects(c.GetPerfects());
                        r.setScore(c.GetScore());
                        r.setMeter(c.GetMeter());
                        r.setRoomId(room.getRoomEntity());
                        r.setStyleId(new cl.stepnxa.persistence.Style(c.GetStyle()));
                        r.setSongId(room.m_Song);

                        PlayerRound pr = new PlayerRound();
                        pr.setPlyrndId(0);
                        pr.setDate(new Date());
                        pr.setPlayerId(c.m_Player);
                        pr.setRoundId(r);

                        try {
                            EntityManagerFactory emf = Persistence.createEntityManagerFactory("StepNXA_ServerPU", System.getProperties());
                            EntityManager em = emf.createEntityManager();
                            em.getTransaction().begin();
                            
                            TypedQuery<Record> q = em.createQuery("SELECT r FROM Record r WHERE r.score > :score AND r.songId=:song_id", Record.class);
                            q.setParameter("score", c.GetScore());
                            q.setParameter("song_id", room.m_Song);
                            
                            Record record = null;
                            try {
                                record = q.getSingleResult();
                            } 
                            catch(Exception e)
                            {                                
                            }
                            
                            if(record == null)
                            {
                                //record
                                Record new_record = new Record();
                                new_record.setPlayerId(c.m_Player);
                                new_record.setSongId(room.m_Song);
                                new_record.setScore(c.GetScore());
                                em.persist(new_record);                                
                            } 
                            
                            if (room.getRoomEntity().getRoomId() == null)
                            {
                                em.persist(room.getRoomEntity());
                            }
                            
                            em.persist(r);
                            em.persist(pr);
                            em.getTransaction().commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    c.HandleGameOver(getClients());
                } else {
                    getClients().remove(c);
                }
            }
        } else {
            System.out.println("One or more players are playing yet");
        }
    }

    /**
     * Envia la actualización de los scores a todos los players
     */
    public void UpdateScoreBoard() {
        for (Client c : getClients()) {
            //System.out.println("GetClients: " + getClients().size());
            c.UpdateScoreBoard(getClients(), 0); //index
            c.UpdateScoreBoard(getClients(), 1); //combos
            c.UpdateScoreBoard(getClients(), 2); //grades            
        }
    }

    /**
     * Comienza el juego y lo informa a los clientes
     */
    public void StartGame() {
        if (m_GameState != GameState.SecondSelect) {
            return;
        }

        m_GameState = GameState.Playing;

        for (Client c : getClients()) {
            c.SelectSong(m_Song, 2);
        }
    }

    /**
     * Seleeciona una canción y lo informa a los clientes
     *
     * @param song Cancion que se jugará
     */
    public void SelectSong(Song song) {
        this.setSong(song);
        this.setGameState(GameState.SecondSelect);

        for (int i = 0; i < getClients().size(); i++) {
            getClients().get(i).SelectSong(m_Song, 1);
        }

        for (int i = 0; i < getClients().size(); i++) {
            if (m_Song.getRanked()) {
                getClients().get(i).SendServerChat("Song Selected: " + m_Song.getTitle() + " by " + m_Song.getArtist() + "\nThis song is ranked");
            } else {
                getClients().get(i).SendServerChat("Song Selected: " + m_Song.getTitle() + " by " + m_Song.getArtist() + "\nThis song is not ranked");
            }
        }
    }

    /**
     * Agrega un cliente al juego
     *
     * @param c El cliente a agregar
     */
    public void AddClient(Client c) {
        m_vClients.add(c);
    }

    /**
     * Devuelve el estado del juego
     *
     * @return El valor de m_GameState
     */
    public GameState getGameState() {
        return m_GameState;
    }

    /**
     * Asignamos nuevo valor a m_GameState
     *
     * @param gamestate El GameState para asignar
     */
    public void setGameState(GameState gamestate) {
        this.m_GameState = gamestate;
    }

    /**
     * Asigna un nuevo song
     *
     * @param song La cancion a asignar
     */
    public void setSong(Song song) {
        this.m_Song = song;
    }

    /**
     * Obtenemos un cliente en la sala por su nombre
     *
     * @param sName El nombre del cliente.
     */
    public Client getClientByName(String sName) {
        for (Client c : m_vClients) {
            if (c.m_sUsername.compareTo(sName) == 0) {
                return c;
            }
        }
        return null;
    }

    /**
     * Obtenemos todos los clientes del juego | thread safe
     *
     * @return Todos los clientes en el juego
     */
    public Vector<Client> getClients() {
        return m_vClients;
    }
}
