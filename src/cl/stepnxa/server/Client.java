package cl.stepnxa.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.Random;
import java.util.Vector;
import cl.stepnxa.persistence.*;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/*
 * ***** URGENTES *****
 *
 * TODO: Testear con más players la seleccion de canciones y las rooms. -> LISTO
 * TODO: Testear con más players si los states funcionan bien. -> LISTO TODO: Si
 * un player que no puede seleccionar cancion selecciona una, queda seleccionada
 * y no pide confirmación cuando el roomadmin selecciona la misma -> LISTO TODO:
 * Al volver de jugar una ronda no se puede seguir jugando -> LISTO TODO:
 * Desconectar apropiadamente a los clientes independiente del estado que tengan
 * y en la sección del juego en la que se encuentren. -> MÁS TESTS TODO: Cuando
 * un player sale de la sala (y estaba solo) la room queda mostrandose y lanza
 * excepcion (nullpointer). -> LISTO TODO: Cuando un cliente sale de la room en
 * medio del gameplay, la room queda tomada como "en juego". -> LISTO TODO:
 * Cuando un player vuelve al lobby y estaba en el screengameplay, ¿que pasa?.
 * -> LISTO TODO: Metricable en StepNXA el PrevScreen del ScreenGameplay. ->
 * LISTO TODO: StepNXA report player options. TODO: Ajustar los scores y los
 * bonus basandose en la información del cliente. TODO: Que todo player tenga su
 * pinger, no que haya un pinger general ->LISTO TODO: Error al salir de una
 * room queda dando nullpointer exception TODO: Que cuando se vuelva de jugar NM
 * no se haga sort basandose en steps types doublesccs TODO: Debemos hacer sort
 * antes de enviar los players? TODO: Crash en 2 player bad server command? ->
 * LISTO TODO: Cuando un player esta esperando la calificación y el otro se va
 * (en medio del gamplay), el q espera nunca recibe datos. y deja esperando
 * eternamente por que el sm no pingea de vuelta. TODO: Cuando un player entra
 * en la sala, refrescar los styles -> LISTO TODO: La rutina de desconexion no
 * funciona -> LISTO TODO: Delegar más responsabilidades al gamemanager sobre el
 * estado del juego TODO: Cuando estamos con una screen overlay, no se refrescan
 * los usuarios
 *
 * ***** URGENTES ***** TODO: Confirmación de recepción de paquetes
 * importantes. -> TEST! TODO: No reiniciar el juego cuando un player entra o
 * sale de la room (incluye a playeroptions) -> TEST!
 *
 * ***** MENOS PRIORITARIOS *****
 *
 * TODO: Battle system con comandos en el chat TODO: Guardar los datos en BD.
 * TODO: Song hash?. TODO: El hash de los caches y el hash de los steps son
 * distintos, pero el sm no detecta cambios cuando modificamos los steps
 * cacheados, tener cuidado con eso. TODO: Login. TODO: Password en las rooms.
 * TODO: Commands en el chat. TODO: Ampliar más el protocolo para saber el lvl
 * del usuario y la diff que está jugando. TODO: WorldMax online.
 */
/**
 * Clase que maneja los paquetes que envia el cliente. También se encarga de
 * enviar las respuesta a él.
 *
 * @version 0.1, 12-04-2012
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Client implements Runnable {

    //socket que mantiene la conexión
    Socket m_Connection;
    //server usa este stream para leer lo que manda el cliente
    DataInputStream m_ToReceivePacket;
    //Server usa este stream para mandar al cliente paquetes
    DataOutputStream m_ToSendPacket;
    //Server usa este stream para mandar al cliente pings y no interrumpir el otro stream
    DataOutputStream m_Pinger;
    //nos dice si está conectado
    boolean m_bConnected = false;
    //0 primer player, 1 segundo player (en el pc que se juega, no en la room)
    int m_iPlayerNumber = 0;
    //Identifica si el usuario ha empezado la sincronización.
    //0 no sincronizado
    //1 sincronizado y listo para empezar a refrescar los scores
    //2 refrescando scores
    int m_iSynchronized = 0;
    //nombre de usuario del cliente
    String m_sUsername;
    /*
     * Status: 0	Inative (no info on this user yet) 1	Active (you know who it
     * is) 2	In Selection Screen 3	In Options 4 In Evaluation
     */
    int m_iState = 1;
    /*
     * 0: exited ScreenNetSelectMusic 1: entered ScreenNetSelectMusic 2: **Not*
     * Sent** 3: entered options screen 4: exited the evaluation screen 5:
     * entered evaluation screen 6: exited ScreenNetRoom 7: entered
     * ScreenNetRoom
     */
    int m_iAction = 0;
    //clase que guarda el estado del player que viene en la BD
    Player m_Player = null;
    //Cuando un player crea una room guardamos el nombre, si es lobby no lo guardamos
    String m_sClientRoomName = "";
    //mantenemos una referencia a la room a la que está el cliente
    Room m_Room = null;
    //el creador de una room puede elegir cancion, el resto no
    boolean m_bRoomAdmin = true;
    //guarda el color que usará el usuario en el chat
    String m_sChatColor = "";
    //dice si el cliente es "lacker"
    boolean m_bLacker = false;
    //Version del protocolo (sirve para los scores)
    int m_iProtocol = 0;
    //Identifica el nombre del build del sm que se conectó
    String m_sBuildName = "";
    //guarda los datos del gameplay del player por cada stage individual
    //cuando se juega una nueva stage, se hacen 0
    PlayerStageStats m_PlayerStats = this.new PlayerStageStats();
    //identifica los style

    enum Style {

        Style_Single,
        Style_HalfDouble,
        Style_Double,
        Style_Couple
    }
    //stepmania enum int's que se leen de los packets
    final int SINGLE = 5;
    final int HDOUBLE = 6;
    final int DOUBLE = 7;
    final int COUPLE = 8; //sin uso pero hay q definir igual
    //identifica los styles

    enum Difficulty {

        Difficulty_Beginner,
        Difficulty_Easy,
        Difficulty_Medium,
        Difficulty_Hard,
        Difficulty_Challenge,
        Difficulty_Edit,
        Difficulty_None,
    }
    //guardan el style, dificultad y nivel respectivamente
    Style m_Style = Style.Style_Single;
    Difficulty m_Difficulty = Difficulty.Difficulty_None;
    int m_iMeter = 0;
    //los dejamos como ints para compatibilizar el networking
    int m_iStyle = 0;
    int m_iDifficulty = 0;
    //Identifica los scores de los steps

    enum TapScore {

        ST_INVALID,
        ST_MINEHIT,
        ST_MINEMISSED,
        ST_MISS,
        ST_W5,//boo
        ST_W4,//good
        ST_W3,//great
        ST_W2,//perfect
        ST_W1,//marv
        ST_LETGO,
        ST_HELD, //ok
        ST_NUMSTEPTYPES
    };
    //identifica el estado de la round

    enum RoundState {

        State_Invalid,
        State_Active,
        State_Ended
    };
    //Guarda los scores por cada stage individualmente

    private class PlayerStageStats {

        int m_iScore;
        int m_iLife; // 0 - 140 in stepnxa
        int m_iMaxCombo;
        int m_iCurCombo;
        int m_iGrade; //sm equivalent enum
        int m_iPlayerID;
        int m_iDifficulty;
        int m_iLevel;
        String m_sOptions;
        int[] TapScores;
        String m_sSongHash; //unused until the server has changed system
        private RoundState m_RoundState = RoundState.State_Invalid;

        public PlayerStageStats() {
            this.m_iScore = 0;
            this.m_iMaxCombo = 0;
            this.m_iLife = 0;
            this.m_iCurCombo = 0;
            this.m_iDifficulty = 0;
            this.m_iGrade = 0;
            this.m_iLevel = 0;
            this.m_iPlayerID = 0;
            this.m_sOptions = "";
            TapScores = new int[10];
        }

        public void UpdateStats(int iScore, int iLife, int iMaxCombo, TapScore score, float fOffset, int iGrade) {
            this.m_iScore = iScore;
            this.m_iLife = iLife;
            this.m_iGrade = iGrade;
            if (iMaxCombo > this.m_iMaxCombo) {
                this.m_iMaxCombo = iMaxCombo;
            }
            this.m_iCurCombo = iMaxCombo;
            switch (score) {
                case ST_W1:
                case ST_W2:
                    TapScores[8]++;
                    break;
                case ST_W3:
                    TapScores[6]++;
                    break;
                case ST_W4:
                    TapScores[5]++;
                    break;
                case ST_W5:
                    TapScores[4]++;
                    break;
                case ST_MISS:
                    TapScores[3]++;
                    break;
                default:
                    break;
            }
            /*
             * for(int i = 0; i < TapScores.length; i++) {
             * System.out.println("TapScore[" + i + "] total: " + TapScores[i]);
             * }
             */
        }

        public void SetPlayerInfo(int iPlayerID, int iDiff, int iLevel, String sOptions) {
            this.m_iPlayerID = iPlayerID;
            this.m_iDifficulty = iDiff;
            this.m_iLevel = iLevel;
            this.m_sOptions = sOptions;
        }

        public void SetPlayerID(int iID) {
            this.m_iPlayerID = iID;
        }

        /**
         * @return the m_RoundState
         */
        public RoundState getRoundState() {
            return m_RoundState;
        }

        /**
         * @param m_RoundState the m_RoundState to set
         */
        public void setRoundState(RoundState m_RoundState) {
            this.m_RoundState = m_RoundState;
        }
    }

    /**
     * Crea un cliente y lo ingresa al server.
     *
     * @param client Es la conexión recibida desde main.
     */
    Client(Socket client) {
        m_Connection = client;
        SetStreams();
        m_bConnected = true;

        int iColorR = new Random().nextInt(120) + 130;
        int iColorG = new Random().nextInt(120) + 130;
        int iColorB = new Random().nextInt(120) + 130;

        String sColorR = Integer.toHexString(iColorR);
        String sColorG = Integer.toHexString(iColorG);
        String sColorB = Integer.toHexString(iColorB);

        m_sChatColor = "|c0" + sColorR + sColorG + sColorB;
        GameManager.getInstance().AddClient(this);

        //
        // TODO: synchronize threads      
        //cada cliente tiene su pinger
        Thread pinger = new Thread(new Runnable() {

            public void run() {
                while (m_bConnected) {
                    PingClient();

                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Stop pinger for " + m_sUsername);
            }
        }, "Pinger");
        pinger.start();
    }

    /**
     * Mueve un cliente y lo mueve a la room padre.
     */
    public void MoveToParent() {
        if (m_Room.getParent().IsLobby()) //si estamos en una room que no es el lobby
        {
            System.out.println("Moving to parent and deleting room if any");
            //por que ser admin del lobby?
            this.m_bRoomAdmin = false;
            //lo sacamos de la room y luego lo metemos al lobby
            if (this.m_Room.m_Game != null) {
                this.m_Room.m_Game.RemoveClient(this);
            }

            m_Room = GameManager.getInstance().RemoveClientFromRoom(m_Room, this);
            //GameManager.getInstance().AddClientToLobby(this);     
            //si la room no tiene clientes la eliminamos
            //if(m_Room != null )
            {
                //if( m_Room.m_vClients.size() <= 0)
                {
                    //GameManager.getInstance().DeleteRoom(m_Room);
                    //this.UpdateRoomList(GameManager.getInstance().getRooms());
                    //updateamos todos los clientes, no solo 1
                    Room lobby = GameManager.getInstance().GetLobby();
                    for (Client c : lobby.getClients()) {
                        c.UpdateRoomList(GameManager.getInstance().getRooms());
                    }
                }
                //else
                if (m_Room != null) {
                    //dejamos al siguiente player como admin room
                    if (!m_Room.getClients().isEmpty()) {
                        m_Room.getClients().get(0).m_bRoomAdmin = true;
                    }
                }
            }
        }
    }

    /**
     * Asigna los streams de la conexión.
     */
    private void SetStreams() {
        try {
            m_ToReceivePacket = new DataInputStream(m_Connection.getInputStream());
            m_ToSendPacket = new DataOutputStream(m_Connection.getOutputStream());
            m_Pinger = new DataOutputStream(m_Connection.getOutputStream());
        } catch (Exception ex) {
            Logger.Trace("Client::SetStreams()");
            ex.printStackTrace();
        }
    }

    /**
     * Desconecta el cliente del server.
     */
    public void Disconnect() {
        try {
            if (m_Room != null) {
                MoveToParent();
                GameManager.getInstance().RemoveClientFromLobby(this);
                GameManager.getInstance().RemoveClient(this);
            } else {
                GameManager.getInstance().RemoveClient(this);
            }
            m_ToReceivePacket.close();
            m_ToSendPacket.close();
            m_Pinger.close();
            m_Connection.close();
            m_bConnected = false;
            GameManager.getInstance().UpdateRoomList();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.Trace(toString() + "::Disconnect()");
        }
    }

    /**
     * Actualiza y lee cuando llegan nuevos paquetes.
     */
    public void Update() {
        byte[] buf = new byte[1020];
        try {
            //si no hay bytes listos para leer salimos.
            //esto bloquea el thread.
            //System.out.println("Available: " + m_ToReceivePacket.available());
            if (m_ToReceivePacket.available() <= 0) {
                return;
            }

            //skip primero 4 bytes (tamaño del packet, ezsockets)
            m_ToReceivePacket.skipBytes(4);
            int iBytesReads = m_ToReceivePacket.read(buf);
            //System.out.println("Bytes readed: " + iBytesReads);
            //ParseData(buf);
            if (iBytesReads != 0) {
                int iCommand = (int) buf[0];
                Response(iCommand, buf);
            }

            //
            // no sirve por que available resuelve el problema.
            /*
             * if (iBytesReads < 0) { this.Disconnect(); m_bConnected = false; }
             *
             */

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Lee desde from hasta que encuentra un 0 en data
     *
     * @param data información leida del paquete
     * @param from indica desde donde va a leer la información
     * @return devuelve la ultima posición leida.
     */
    public int GetStringLastPos(byte[] data, int from) {
        int i = from;
        //String sOut = "";
        while (i < 1020 && ((char) data[i] != 0)) {
            //sOut += String.valueOf((char)data[i]);
            i++;
        }

        return i;
    }

    /**
     * Lee desde from hasta que encuentra un 0 en data
     *
     * @param data Información leida desde el paquete
     * @param from indica desde donde va a leer la información
     * @return devuelve el string que lee desde data
     */
    public String GetStringNT(byte[] data, int from) {
        int i = from;
        String sOut = "";
        while (i < 1020 && ((char) data[i] != 0)) {
            sOut += String.valueOf((char) data[i]);
            i++;
        }

        return sOut;
    }

    /**
     * Lee paquetes con formato de SMO
     *
     * @param data Información leida desde el paquete
     */
    public void SMOParseData(byte[] bData) {
        //String str = new String(bData);
        //System.out.println(str);

        //empezamos leyendo desde 1, puesto que el 1 es el comando
        int iSMOCommand = (int) bData[1];
        switch (iSMOCommand) {
            case 0: //login
            {
                m_iPlayerNumber = (int) bData[2];
                int iHash = (int) bData[3]; //0 md5, 1 md5 + salt
                //System.out.println("String leido: " + GetStringNT(bData, 4, 1020));                
                int iLastPos = GetStringLastPos(bData, 4);
                m_sUsername = GetStringNT(bData, 4);
                String sPass = GetStringNT(bData, iLastPos + 1);

                //XXX: login in BD
                //0 is approved
                //m_Player = MySQLManager.getInstance().getPlayerByUsernameAndPassword(m_sUsername, sPass);
                System.out.println("PASS: " + sPass);
                Player p = Player.getLogin(m_sUsername, sPass);
                if (p == null) {
                    PutBytes(PacketUtils.LoginResponse("Bad username or password", 1));
                } else {
                    m_Player = p;
                    m_Player.setUsername(m_sUsername);
                    m_Player.setPassword(sPass);
                    PutBytes(PacketUtils.LoginResponse("Welcome to the server", 0));
                }
                Send();
                break;
            }
            case 2: { //Crea una sala de <b>juego</b>
                int iRoomType = (int) bData[2];
                String sRoomTitle = GetStringNT(bData, 3);
                int iLastPos = GetStringLastPos(bData, 3);
                String sRoomDesc = GetStringNT(bData, iLastPos + 1);
                iLastPos = GetStringLastPos(bData, iLastPos + 1);
                String sPass = GetStringNT(bData, iLastPos + 1);
                //System.out.println("Title: " + sRoomTitle);
                //System.out.println("Desc: " + sRoomDesc);
                //System.out.println("Pass: " + sPass);
                String s = "pump\0";
                //podemos enviar cualquier modo jijiji -joke
                String p = "single\0";
                Write4b(1 + s.length() + p.length());
                Write1b(138);
                WriteString(s);
                WriteString(p);
                /*
                 * try { m_ToSendPacket.writeBytes(s);
                 * m_ToSendPacket.writeBytes(p); } catch (Exception e) {
                 * e.printStackTrace(); }
                 */
                Send();

                Room r = new Room(1 /*
                         * 0 chat room, 1 game room
                         */, sRoomTitle, sRoomDesc, sPass, false);
                //todas las rooms tienen ese parent
                r.setParent(GameManager.getInstance().GetLobby());
                m_sClientRoomName = sRoomTitle;
                m_bRoomAdmin = true; //nos aseguramos que es admin
                this.m_Room = r;
                GameManager.getInstance().AddRoom(r);
                GameManager.getInstance().AddClientToRoom(r, this);
                break;
            }
            case 1: //user quiere entrar a una sala
            {
                int iAction = (int) bData[2]; // 0 exit, 1 enter
                String sRoomTitle = GetStringNT(bData, 3);
                int iLastPos = GetStringLastPos(bData, 3);
                String sPass = GetStringNT(bData, iLastPos);
                this.m_bRoomAdmin = false;
                //seteamos el roomname para todos los players
                this.m_sClientRoomName = sRoomTitle;

                //System.out.println("User wish enter to room: " + sRoomTitle + " with pass: " + sPass);
                if (iAction == 1) //enter to room
                {
                    //
                    // Compare pass
                    Room r = GameManager.getInstance().GetRoomByName(sRoomTitle);
                    if( r.m_sPassword.compareTo(sPass) == 0 )
                    {
                        //si estamos jugando dentro de la sala, no se puede entrar
                        if (r.m_Game == null || r.m_Game.getGameState() != Game.GameState.Playing) {
                            this.m_Room = r;
                            GameManager.getInstance().AddClientToRoom(r, this);
                            String s = "pump\0";
                            String p = "single\0";
                            Write4b(1 + s.length() + p.length());
                            Write1b(138);
                            WriteString(s);
                            WriteString(p);
                            Send();
                        } else {
                            SendServerChat("The Room is in game");
                        }
                    }
                    else
                    {
                        SendServerChat("Bad password");
                    }
                } 
                else if (iAction == 0) //exit room
                {
                    //Room r = GameManager.getInstance().GetRoomByName(sRoomTitle);
                    //GameManager.getInstance().RemoveClientFromRoom(r, this);                    
//                    if(m_Room != null)
//                    {
//                        //XXX: que pasa si salimos de la room y hay players jugando?
//                        //if( m_Room.m_Game.getGameState() != Game.GameState.Playing )
//                        {
//                            MoveToParent();
//                            System.out.println("Exiting room: " + m_sUsername);
//                        }
//                    }
                }
                break;
            }
            case 3: //entrega al usuario información de la room que está seleccionada
            {
                String sRoomName = GetStringNT(bData, 2);
                SendRoomInfo(sRoomName);
                break;
            }
            default:
                Logger.Trace("Commando SMO desconocido: " + iSMOCommand);
        }
    }

    /**
     * Responde al comando recibido desde el cliente
     *
     * @param iCommand Comando leido al que hay que responder
     * @param bData Información leida desde el paquete
     */
    public void Response(int iCommand, byte[] bData) {
        System.out.println("Responsing to command: " + iCommand);
        switch (iCommand) {
            case 2: //hello client = 002 | server = 130
            {
                int iProtocol = bData[1];
                String sBuild = GetStringNT(bData, 2);
                int iLastPos = GetStringLastPos(bData, 2);
                String sMd5 = GetStringNT(bData, iLastPos + 1);

                this.m_iProtocol = iProtocol;
                this.m_sBuildName = sBuild;
                //sm protocol to stepnxa = 31 /* preview */
                System.out.println("Protocol: " + m_iProtocol + " SM version: " + m_sBuildName);
                System.out.println("MD5: " + sMd5);

                if (!Executable.testExecutableMD5(sMd5)) {
                    //System.out.println("Tu exe no es valido.");
                    Disconnect();
                    return;
                }

                PutBytes(PacketUtils.Hello("StepNXA Server"));
                Send();
            }
            break;
            case 6: //style update
            {
                int iPlayersEnabled = (int) bData[1];
                int iPlayerNumber = (int) bData[2];
                String sPlayerName = GetStringNT(bData, 3);
                System.out.println("PlayerNumber: " + iPlayerNumber);
                System.out.println("Name: " + sPlayerName);
            }
            break;
            case 12: //smoparse data
            {
                //
                //  Primero, leer el paquete
                //
                SMOParseData(bData);
            }
            break;
            case 10: { //salir o entrar a screens
                m_iAction = (int) bData[1];
                System.out.println("Command 10: Action: " + m_iAction);
                //bellow don't go, because the addroom send that data                
                switch (m_iAction) {
                    //entered to screennetroom
                    case 7: {
                        Room lobby = GameManager.getInstance().GetRoomByName("Lobby");
                        m_sClientRoomName = "";
                        m_Room = lobby;
                        GameManager.getInstance().AddClientToRoom(lobby, this);
                        GameManager.getInstance().UpdateRoomList();
                        break;
                    }
                    //exited screennetroom
                    case 6: {
                        //Room lobby = GameManager.getInstance().GetRoomByName("Lobby");
                        //GameManager.getInstance().RemoveClientFromRoom(lobby, this);
                        //m_sClientRoomName = "";
                        m_Room = null;
                        GameManager.getInstance().RemoveClientFromLobby(this);
                        break;
                    }
                    // entered screennetselectmusic
                    case 1: {

                        //quitamos el cliente del juego aqui
                        if (m_Room.m_Game != null && this.m_iState == 1) {
                            m_Room.m_Game.RemoveClient(this);
                        }
                        //System.out.println("Entrando a selectmusic");
                        //TODO: se reinicia si tenemos una cancion seleccionada (class game instanciada)
                        //      y si hay más players no se puede jugar
                        if (m_Room.m_Game != null && m_Room.m_Game.getClients().size() <= 0 && this.m_iState == 1) {
                            //XXX: Si hay players jugando, reiniciamos el juego?
                            //si podemos reiniciar el juego
                            //boolean bReinitable = true; 
                            //quitamos el cliente del juego aqui
                            //m_Room.m_Game.RemoveClient(this);
                            //si hay mas clientes, entonces no podemos reiniciar el juego.
                            //if( m_Room.m_Game.getClients().size() > 1 )                   
                            //  bReinitable = false;                  

                            //if( bReinitable )
                            {
                                System.out.println("(Re)initing the game");
                                m_Room.m_iStatus = 1; //normal
                                GameManager.getInstance().UpdateRoomList();
                                m_Room.m_Game = new Game();
                            }
                        }
                        //m_Room.setClientState(m_sUsername, 1);
                        this.m_iState = 1;
                        m_Room.UpdateUserList();
                        GameManager.getInstance().RemoveClientFromLobby(this);
                        break;//? borramos del lobby
                    }
                    // exited screennetselectmusic
                    case 0: {
                        //Room r = GameManager.getInstance().GetRoomByName(sRoomTitle);
                        //GameManager.getInstance().RemoveClientFromRoom(r, this);
                        //System.out.println("Exiting room: " + m_sUsername);

                        //XXX: if room set to in game, then not to delete
                        if (m_Room != null) {
                            //System.out.println("Salimos de netselmusic");
                            if (m_Room.m_iStatus != 2) {
                                MoveToParent();
                            }
                        }
                        break;
                    }
                    //enter to screenoptions
                    case 3: {
                        //el usuario está en las opciones
                        //System.out.println("Entering in ScreenOptions");
                        //m_Room.setClientState(m_sUsername, 3);
                        //System.out.println("Enter to options");
                        this.m_iState = 3;
                        //actualizamos la lista de usuarios
                        m_Room.UpdateUserList();
                        break;
                    }
                    //exited evaluation screen    
                    case 4: {
                        //m_Room.setClientState(m_sUsername, 1);
                        m_iState = 1;
                        m_Room.m_Game.setGameState(Game.GameState.NONE);
                        //cuando salimos del gameplay quedamos en estado de ronda invalido
                        this.m_PlayerStats.setRoundState(RoundState.State_Invalid);
                        break;
                    }
                    //entered evaluation screen
                    case 5: {
                        //todos los clientes tienen que hacer eso?
                        //m_Room.m_Game = new Game();
                        //m_Room.setClientState(m_sUsername, 4);
                        this.m_iState = 4;
                        break;
                    }
                    //enter gameplay screen
                    case 8: {
                        //XXX: entering gameplay, NullPointerException
                        m_Room.m_Game.setGameState(Game.GameState.Playing);
                        break;
                    }
                    //exit gameplay screen
                    case 9: {
                        //si volvemos del screengameplay y no mandamos las stats
                        //debemos matar el updater de este player.
                        m_iSynchronized = 0;
                        this.m_PlayerStats.setRoundState(RoundState.State_Ended);
                        break;
                    }
                }
            }
            break;
            case 8: { //seleccionar una canción para jugar
                /*
                 * Usage of message 0: (in response to server 8) User has
                 * specified song 1: (in response to server 8) User does NOT
                 * have specified song 2: User requested a start game on given
                 * song NT	Song Title (As gotten by GetTranslitMainTitle) NT
                 * Song Artist (As Gotten by GetTranslitArtist) NT	Song Subtitle
                 * (As gotten by GetTranslitSubTitle) NT Hash for the steps file
                 */
                int iSubCommand = (int) bData[1];
                String sTitle, sArtist, sSubTitle, sHash;
                sTitle = GetStringNT(bData, 2);
                int iLastPos = GetStringLastPos(bData, 2);
                sArtist = GetStringNT(bData, iLastPos + 1);
                iLastPos = GetStringLastPos(bData, iLastPos + 1);
                sSubTitle = GetStringNT(bData, iLastPos + 1);
                iLastPos = GetStringLastPos(bData, iLastPos + 1);
                sHash = GetStringNT(bData, iLastPos + 1);

                Room r = m_Room;
                r.SetLastSong(sTitle, sArtist, sHash);
                //System.out.println("MD5: " + sHash);
                //System.out.println(r.m_Song.getExistInServer() == true ? "Cancion existe en el server." : "Cancion no existe");

                //SelectSong(r.m_Song);
                switch (iSubCommand) {
                    case 2: {
                        if (this.m_bRoomAdmin) {
                            //mira si hay algun player todavia "en juego"
                            for (Client c : m_Room.getClients()) {
                                //XXX: revisar si seleccionamos una canción y no nos debe dejar si hay alguien jugando.
                                //XXX: es esta la mejor forma de hacerlo?                                
                                //if(m_Room.m_Game != null && m_Room.m_Game.getClients().size() > 0 )
                                if (c.m_iState != 1) {
                                    //XXX: o kickealo
                                    SendServerChat("No se puede comenzar la ronda por que hay players jugando todavia, espera a que terminen.");
                                    return;
                                }
                            }
                            r.SelectSong(iSubCommand);
                        } else {
                            SendServerChat("No tienes permiso para seleccionar cancion");
                        }
                    }
                    break;
                    case 1: {
                        //m_Room.SendChat();
                        //SendChat("No tienes la canción para jugar");
                        //System.out.println("Cliente no tiene la cancion");                        
                        //esto deberia pertenecer a Room
                        for (int i = 0; i < m_Room.getClients().size(); i++) {
                            m_Room.getClients().get(i).SendServerChat("El player: " + this.m_sUsername + " no tiene la cancion");
                        }
                        m_Room.getClientByName(m_sUsername).m_bLacker = true;
                        m_Room.m_Game.getClientByName(m_sUsername).m_bLacker = true;
                        return;
                    }
                    case 0: {
                        m_Room.getClientByName(m_sUsername).m_bLacker = false;
                        m_Room.m_Game.getClientByName(m_sUsername).m_bLacker = false;
                        //System.out.println("Cliente "+ m_sUsername +" tiene la cancion");
                    }
                    break;
                }

                //UpdateUserList(GameManager.getInstance().m_vClients);

                //
                //  En este punto se puede filtrar por el hash
                //  y comparar lo que envia el cliente y lo que hay
                //  en el servidor y tener songs estandarizadas
                //
                //System.out.println("title: " + sTitle + " artist: " + sArtist);
                //System.out.println("Command: " + iSubCommand);
                //System.out.println("Hash: " + sHash);//1144216616
            }
            break;
            case 7: //chat
            {
                String sText = GetStringNT(bData, 1);
                if( !sText.startsWith("/"))
                {
                    if (this.m_Room.IsLobby()) //lobby room
                    {
                        Room r = GameManager.getInstance().GetLobby();
                        r.SendChat(sText, this);
                    } else {
                        Room r = m_Room;
                        r.SendChat(sText, this);
                    }
                }
                else //Nos enviaron un command
                {
                    //la estructura de los commandos es asi:
                    // /comando opciones1 opcion2 opcionN
                }
            }
            break;
            //game start!
            case 3: {
                GameStartRequestHandle(bData);
                switch (m_iSynchronized) {
                    case 0:
                        m_iSynchronized++;
                        break;
                    case 1:
                        m_iSynchronized++;
                        break;
                    case 2:
                        break;
                }
                //if(m_iSynchronized == 2)
                //    System.out.println("Refreshing scores");
            }
            break;
            //gameplay update scores
            case 5: {
                /*
                 * if( m_Room.m_Game.getGameState() != Game.GameState.Playing )
                 * { return; }
                 *
                 */
                GameStatsUpdate(bData);

                // TODO: synchronize threads
                if (m_iSynchronized == 2) {
                    this.m_PlayerStats.setRoundState(RoundState.State_Active);
                    m_iSynchronized++;
                    Thread updater = new Thread(new Runnable() {

                        public void run() {
                            while (m_iSynchronized == 3) {
                                Vector<Client> c = m_Room.m_Game.getClients();
                                if (c != null) {
                                    //System.out.println("Pinging #Clients: " + c.size());
                                    for (int i = 0; i < c.size(); i++) {
                                        if (c.get(i).m_bConnected) {
                                            c.get(i).UpdateScoreBoard(c, 0);
                                            c.get(i).UpdateScoreBoard(c, 1);
                                            c.get(i).UpdateScoreBoard(c, 2);
                                        }
                                    }
                                }

                                try {
                                    Thread.sleep(2000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            //System.out.println("Quitting updater for: " + m_sUsername);
                        }
                    }, "Updater");
                    updater.start();
                }
            }
            break;
            //Response a GameOver Stats
            case 4: {
                //XXX: si no manda este mensaje, el updater no se muere y queda tomado.
                m_iSynchronized = 0;
                //m_Room.m_Game.setGameState(Game.GameState.NONE);
                //que pasa si no hay stats todavia para todos los players?
                //XXX:
                this.m_PlayerStats.setRoundState(RoundState.State_Ended);

                m_Room.m_Game.HandleGameOver(this.m_Room);
                //System.out.println("Sended gameover stats");
            }
            break;
            case 18: {
                int iStyletype = bData[1];
                int iDiff = bData[2];
                int iMeter = bData[3];

                switch (iStyletype) {
                    case SINGLE:
                        m_Style = Style.Style_Single;
                        break;
                    case HDOUBLE:
                        m_Style = Style.Style_HalfDouble;
                        break;
                    case DOUBLE:
                        m_Style = Style.Style_Double;
                        break;
                    case COUPLE:
                        m_Style = Style.Style_Couple;
                        break;
                }

                m_Difficulty = Difficulty.values()[iDiff];
                m_iMeter = iMeter;
                m_iDifficulty = iDiff;
                m_iStyle = iStyletype;

                this.m_Room.UpdateStylesAndDifficulties();
                System.out.println("Styles info sended for " + m_sUsername);
            }
            break;
            default: {
                //System.out.println("Fallback command: " + iCommand);
            }
        }
    }

    public void SendUpdateStylesAndDifficulties(Vector<Client> c) {
        Vector<Client> clients = (Vector<Client>) c.clone();
        for (int i = 0; i < clients.size(); i++) {
            try {
                if (!clients.get(i).m_bConnected) {
                    clients.remove(i);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int iSize = 0;
        iSize += 2; //server command and # of players
        for (int i = 0; i < clients.size(); i++) {
            iSize += clients.get(i).m_sUsername.length() + 1;
        }
        iSize += 3 * clients.size();

        Write4b(iSize);//size first!!
        Write1b(146);//update diffs
        Write1b(clients.size());
        for (int i = 0; i < clients.size(); i++) {
            WriteString(clients.get(i).m_sUsername + "\0");
        }

        for (int i = 0; i < clients.size(); i++) {
            Write1b(clients.get(i).m_iStyle);
        }

        for (int i = 0; i < clients.size(); i++) {
            Write1b(clients.get(i).m_iDifficulty);
        }

        for (int i = 0; i < clients.size(); i++) {
            Write1b(clients.get(i).m_iMeter);
        }
    }

    /**
     * Manda los scores finales a todos los players
     *
     * @param c Todos los clientes del juego (en la sala)
     */
    public void HandleGameOver(Vector<Client> c) {
        //score bonuses!
        //TODO:
        /*
         * if( this.m_iMeter >= 10 ) { System.out.println("Level: " +
         * this.m_iMeter); this.m_PlayerStats.m_iScore *=
         * (this.m_PlayerStats.m_iLevel/10.f); }
         *
         * System.out.println("Score: " + this.m_PlayerStats.m_iScore);
         *
         * if( (this.m_Difficulty == Difficulty.Difficulty_Hard ||
         * this.m_Difficulty == Difficulty.Difficulty_Challenge ) &&
         * this.m_Style == Style.Style_Double ) { this.m_PlayerStats.m_iScore +=
         * 500000; }
         *
         * System.out.println("Score: " + this.m_PlayerStats.m_iScore);
         *
         * if( this.m_PlayerStats.m_iGrade <= 1 ) this.m_PlayerStats.m_iScore +=
         * 100000;
         *
         * System.out.println("Score: " + this.m_PlayerStats.m_iScore);
         *
         * SystemMessage("Final score for me: " + this.m_PlayerStats.m_iScore);
         */
      
        Object[] c2 = c.toArray();
        //Arrays.sort(c2, comp);

        int iSize = 0;
        iSize += c2.length;//ids
        iSize += c2.length * 4;//scores
        iSize += c2.length;//grade
        iSize += c2.length;//diff
        iSize += (c2.length * 2) * 8;//all other scores
        iSize += 2;//server command and number of players
        //player options
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            //WriteString(cl.m_PlayerStats.m_sOptions+ "\0");
            iSize += cl.m_PlayerStats.m_sOptions.length() + 1;
        }

        Write4b(iSize); //size first!
        Write1b(132); //server command
        Write1b(c2.length);

        //first ids
        for (int i = 0; i < c2.length; i++) {
            Write1b(i);
        }
        //2nd scores
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write4b(cl.m_PlayerStats.m_iScore);
        }
        //grades
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write1b(cl.m_PlayerStats.m_iGrade);
        }
        //difficulty
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write1b(cl.m_PlayerStats.m_iDifficulty);
        }
        //per tap-Score values
        /*
         * switch(score) { case ST_W1: case ST_W2: TapScores[8]++; break; case
         * ST_W3: TapScores[6]++; break; case ST_W4: TapScores[5]++; break; case
         * ST_W5: TapScores[4]++; break; case ST_MISS: TapScores[3]++; break;
         * default: break; }
         */
        //miss
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write2b(cl.m_PlayerStats.TapScores[3]);
        }
        //bad
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write2b(cl.m_PlayerStats.TapScores[4]);
        }
        //good
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write2b(cl.m_PlayerStats.TapScores[5]);
        }
        //great
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write2b(cl.m_PlayerStats.TapScores[6]);
        }
        //perfect
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write2b(cl.m_PlayerStats.TapScores[8]);
        }
        //marvelous, lo enviamos para compatibilizar solamente
        for (int i = 0; i < c2.length; i++) {
            //Client cl = (Client)c2[i];
            Write2b(0);
        }

        //ok - lo mismo que marveloues, solo para compatibilizar
        for (int i = 0; i < c2.length; i++) {
            //Client cl = (Client)c2[i];
            Write2b(0);
        }
        //maxcombo
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            Write2b(cl.m_PlayerStats.m_iMaxCombo);
        }
        //player options
        for (int i = 0; i < c2.length; i++) {
            Client cl = (Client) c2[i];
            WriteString(cl.m_PlayerStats.m_sOptions + "\0");
        }        
    }

    /**
     * Actualiza las "stats" de los players
     */
    public void GameStatsUpdate(byte[] bData) {
        //leemos desde 1 hacia adelante
        ByteBuffer b = ByteBuffer.wrap(bData);
        //System.out.println("ByteBuff Length: " + b.capacity());

        b.get(); // no nos interesa leer denuevo el comando
        byte _byte = b.get();
        short pn = (short) (_byte / 16);
        short iStepType = (short) (_byte % 16);
        _byte = b.get();
        short iGrade = (short) (_byte / 16);

        int iScore = b.getInt();
        int iCombo = b.getShort();
        int iLife = b.getShort();
        int iOffset = b.getShort();

        float fOffset = (float) ((iOffset / 2000.0f) - 16.384f);
        //fix para los offset altos, con esto vuelven a ser 0.algo
        if (fOffset > -33 && fOffset < -30) {
            fOffset = (float) (fOffset + (16.384f * 2f));
        }

        TapScore TapsScores = TapScore.values()[iStepType];
        this.m_PlayerStats.UpdateStats(iScore, iLife, iCombo, TapsScores, fOffset, iGrade);

    }

    /**
     * Manda las actualizaciones de los scores de los clientes al cliente
     *
     * @param iSection 0 actualiza nombres, 1 actualiza scores, 2 actualiza
     * calificación
     */
    public void UpdateScoreBoard(Vector<Client> c, int iSection) {
        //System.out.println("Update: " + c.size() + " players");
        PutBytes(PacketUtils.UpdateScoreBoard(c, iSection));
        Send();
    }

    /**
     * Recepciona el paquete de inicio de juego desde el cliente.
     *
     * @param bData Array de datos desde el cliente
     */
    public void GameStartRequestHandle(byte[] bData) {
        byte _byte = bData[1];
        short p1feet = (short) (_byte / 16);
        short p2feet = (short) (_byte % 16);

        _byte = bData[2];
        short p1diff = (short) (_byte / 16);
        short p2diff = (short) (_byte % 16);

        _byte = bData[3];
        short iStartPos = (short) (_byte % 16);

        //System.out.println("Syncing Player: " + m_sUsername + " State: " + iStartPos);

        if (iStartPos == 0) {
            //send sync and no return            
            SyncStart();
        } else if (iStartPos == 1) {
            //send sync and return            
            SyncStart();
            return;
        }

        //System.out.println("Preparing StageStats");
        m_PlayerStats = this.new PlayerStageStats();

        //obtenemos la información del cliente en el juego
        //
        //Hash ?
        String sTitle = GetStringNT(bData, 4);
        int iLastPos = GetStringLastPos(bData, 4);
        String sSub = GetStringNT(bData, iLastPos + 1);
        iLastPos = GetStringLastPos(bData, iLastPos + 1);
        String sArtist = GetStringNT(bData, iLastPos + 1);
        iLastPos = GetStringLastPos(bData, iLastPos + 1);
        String sCourse = GetStringNT(bData, iLastPos + 1);
        iLastPos = GetStringLastPos(bData, iLastPos + 1);
        String sSongOps = GetStringNT(bData, iLastPos + 1);
        iLastPos = GetStringLastPos(bData, iLastPos + 1);
        String s1pOps = GetStringNT(bData, iLastPos + 1);
        iLastPos = GetStringLastPos(bData, iLastPos + 1);
        String s2pOps = GetStringNT(bData, iLastPos + 1);

        //si el nivel es mayor que 0 está jugando
        if (p1feet > 0) {
            //System.out.println("Player 1: Diff: " + p1diff + " lvl: " + p1feet + " title: " + sTitle + " Ops: " + s1pOps + " SOps: " + sSongOps + " Artist: " + sArtist);
            this.m_PlayerStats.SetPlayerInfo(0, p1diff, p1feet, s1pOps);
        }

        //si el nivel es mayor que 0 está jugando
        if (p2feet > 0) {
            //System.out.println("Player 2: Diff: " + p2diff + " lvl: " + p2feet + " title: " + sTitle + " Ops: " + s2pOps);
            this.m_PlayerStats.SetPlayerInfo(1, p2diff, p2feet, s2pOps);
        }
    }

    /**
     * Enviamos al cliente un paquete de sincronización para esperar a los demás
     * players
     */
    public void SyncStart() {
        PutBytes(PacketUtils.AllowStart());
        Send();
    }

    /**
     * Envia al cliente un aviso que una canción fue seleccionada
     *
     * @param s Clase que contiene la información de la canción que fue
     * seleccionada
     * @param iSubCommand Comando a enviar al cliente
     */
    public void SelectSong(Song s, int iSubCommand) {
        /*
         * ********** CLIENT ************ 008:	Request Start Game and Tell
         * server existance/non existance of song. Desc:	The user selected a
         * song on a Net-enabled selection Size: 1	Usage of message 0: (in
         * response to server 8) User has specified song 1: (in response to
         * server 8) User does NOT have specified song 2: User requested a start
         * game on given song NT	Song Title (As gotten by GetTranslitMainTitle)
         * NT	Song Artist (As Gotten by GetTranslitArtist) NT	Song Subtitle (As
         * gotten by GetTranslitSubTitle) NT Dir hash -> RageFileMan (if changes
         * doesn't match with de server songs hash)
         */
        /*
         * ********** SERVER *********** 008:(136)Tell client to start song/ask
         * if client has song Desc:	The user selected a song on a Net-enabled
         * selection Size: 1	Usage of message 0: See if client has song 1: See
         * if client has song, if so, scroll to song 2: See if client has song,
         * if so, scroll to song, and play that song 3: Blindly start song NT
         * Song Title (As gotten by GetTranslitMainTitle) NT	Song Artist (As
         * Gotten by GetTranslitArtist) NT	Song Subtitle (As gotten by
         * GetTranslitSubTitle)
         */
        int iSize = 0;
        iSize += s.getTitle().length() + 1;
        iSize += s.getArtist().length() + 1;
        iSize += 3; //subtitle, server command, subcomand
        Write4b(iSize);//size
        Write1b(136);//server command
        Write1b(iSubCommand);//See if client has song, if so, scroll to song
        WriteString(s.getTitle() + "\0");
        WriteString(s.getArtist() + "\0");
        WriteString("\0");
        Send();
        //System.out.println("Sended command: " + iSubCommand);
    }

    /**
     * Envia un ping al cliente
     */
    public void PingClient() {
        try {
            //m_Pinger.writeInt(1);
            //m_Pinger.writeByte(128);
            m_Pinger.write(PacketUtils.Ping());
            m_Pinger.flush();
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Manda un mensaje de sistema al cliente
     *
     * @param sMessage Mensaje a enviar al cliente
     */
    public void SystemMessage(String sMessage) {
        PutBytes(PacketUtils.SystemMessage(sMessage));
        Send();
    }

    /**
     * Envia la información de una sala en particular al cliente
     *
     * @param sRoomName Nombre de la room
     */
    public void SendRoomInfo(String sRoomName) {
        /*
         * 003: Room Info Size: NT	Last Song Title NT Last Song Subtitle NT Last
         * Song Artist 1	Num Players 1	Max Players NT	Player1 Name ... NT
         * PlayerN Name
         */
        Room r = GameManager.getInstance().GetRoomByName(sRoomName);

        int iSize = 0;
        //WriteString(r.m_Song.getTitle().concat("\0"));
        iSize += r.m_Song.getTitle().length() + 1;
        //WriteString("\0");//test!
        iSize++;
        //WriteString(r.m_Song.getArtist().concat("\0"));
        iSize += r.m_Song.getArtist().length() + 1;
        //Write1b(r.m_vClients.size());
        iSize++;
        //Write1b(32);//max players
        iSize++;
        for (int i = 0; i < r.getClients().size(); i++) {
            iSize += r.getClients().get(i).m_sUsername.length() + 1;
            //WriteString(r.m_vClients.get(i).m_sUsername.concat("\0"));
        }
        //server and smo command
        iSize += 2;

        Write4b(iSize);//size first!
        Write1b(140);//server command
        Write1b(3);//smo command
        WriteString(r.m_Song.getTitle().concat("\0"));
        WriteString("\0");//test!
        WriteString(r.m_Song.getArtist().concat("\0"));
        Write1b(r.getClients().size());
        Write1b(32);//max players
        for (int i = 0; i < r.getClients().size(); i++) {
            WriteString(r.getClients().get(i).m_sUsername.concat("\0"));
        }
    }

    /**
     * Actualiza la lista de todas las salas activas
     *
     * @param r Lista de las salas activas
     */
    public void UpdateRoomList(Vector<Room> r) {

        PutBytes(PacketUtils.UpdateAllRoms());
        Send();
        /*
         *
         * ACTUALMENTE SOLO NOS INTERESA ACTUALIZAR LAS ROOMS, NO MODIFICARLAS
         *
         *
         * 001: Room Update (Changing rooms) Size: 1	Type of update 0: Change
         * Room Title 1: Update List of other rooms (or games)
         *
         * If Room List Update: 1	Number of rooms NT	Room1 Title NT	Room1
         * Description NT	Room2 Title NT	Room2 Description ... NT	RoomN Title NT
         * RoomN Description
         *
         * Room Status appended for reverse compatibility 1	Room1 Status 0:
         * Normal Room 1: Unused 2: Room in game 3: First stage of song
         * selection has been done 4: Second stage of song selection has been
         * done 1	Room2 Status ... 1	RoomN Status
         *
         * Room Flags appended for reverse compatibility 1	Room1 Flags bit 0:
         * Passworded if true 1	Room2 Status ... 1	RoomN Status
         */
    }

    /**
     * Actualiza la lista de clientes
     *
     * @param c Lista de clientes
     */
    public void UpdateUserList(Vector<Client> c) {
        if (c == null) {
            return;
        }

        PutBytes(PacketUtils.UpdateUserList(c));
        Send();

        //System.out.println("Sended: " + m_ToSendPacket.size() + " alloqued: " + iSize);
    }

    /**
     * Envia un mensaje al chat del cliente
     *
     * @param sChat Mensaje a enviar al cliente
     * @param sOwner El nombre del usuario que envió el mensaje
     */
    public void SendChat(String sChat, Client Owner) {
        String sFullChat = Owner.m_sChatColor + Owner.m_sUsername + " |c0ffffff: " + sChat;
        PutBytes(PacketUtils.ChatMessage(sFullChat));
        Send();
    }

    /**
     * Envia un mensaje de algún evento a los usuario (usualmente)
     *
     * @param sChat Mensaje a enviar al cliente
     */
    public void SendServerChat(String sChat) {
        String sFullChat = "|c0ffffff " + sChat;
        /*
         * 007:(135)Chat Message Desc:	Add a chat message to the chat window on
         * some StepMania screens. Payload: Size	Description NT	Message
         */
        int iSize = 0;
        iSize = sFullChat.length() + 1;
        iSize++;
        Write4b(iSize);//size!
        Write1b(135);//server command
        WriteString(sFullChat.concat("\0"));
        Send();
    }

    /**
     * Comienza la ejecución del thread
     */
    public void run() {
        do {
            Update();
            //PingClient();
            try {
                Thread.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (m_bConnected);
        System.out.println("Client Disconected");
        //GameManager.getInstance().RemoveClient(this);
    }

    /**
     * Envia la información del paquete al cliente
     */
    public void Send() {
        try {
            m_ToSendPacket.flush();
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    public void PutBytes(byte[] b) {
        try {
            m_ToSendPacket.write(b);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream 1 byte
     *
     * @param b Información que se enviará al cliente
     */
    public void Write1b(int b) {
        byte a = (byte) b;
        try {
            m_ToSendPacket.writeByte(a);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream 2 bytes
     *
     * @param b Información que se enviará al cliente
     */
    public void Write2b(int b) {
        short a = (short) b;
        try {
            m_ToSendPacket.writeShort(a);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream 4 byte
     *
     * @param b Información que se enviará al cliente
     */
    public void Write4b(int b) {
        try {
            m_ToSendPacket.writeInt(b);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }

    /**
     * Escribe en el Stream s.Length() bytes
     *
     * @param s Información que se enviará al cliente
     */
    public void WriteString(String s) {
        //System.out.println("String: " + s + " size " + s.length());
        try {
            m_ToSendPacket.writeBytes(s);
        } catch (Exception e) {
            Disconnect();
            e.printStackTrace();
        }
    }                                         

    public int GetScore() {
        return m_PlayerStats.m_iScore;
    }
    
    public int GetStyle() {        
        if( m_Style == Style.Style_Single )
            return 1;
        else
            return 2;
    }
    
    public int GetMeter() {
        return this.m_PlayerStats.m_iLevel;
    }

    public int GetCombo() {
        return m_PlayerStats.m_iCurCombo;
    }

    public int GetMaxCombo() {
        return m_PlayerStats.m_iMaxCombo;
    }

    public int GetGrade() {
        return m_PlayerStats.m_iGrade;
    }

    public RoundState GetRoundState() {
        return m_PlayerStats.m_RoundState;
    }
    
    public int GetMisses() {
        return this.m_PlayerStats.TapScores[3];               
    }
    
    public int GetBads() {
         return this.m_PlayerStats.TapScores[4];                
    }
    
    public int GetGoods() {
        return this.m_PlayerStats.TapScores[5];                
    }
    
    public int GetGreats() {
        return this.m_PlayerStats.TapScores[6];                
    }
    
    public int GetPerfects() {
        return this.m_PlayerStats.TapScores[8];                
    }
}
