package cl.stepnxa.server;

import cl.stepnxa.persistence.Player;
import java.util.Vector;
import cl.stepnxa.persistence.Song;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * Clase que mantiene los clientes y la sala
 *
 * @version 0.1, 12-04-2012
 * @author Víctor Gajardo Henríquez (v1toko)
 */
public class Room {

    //Mantiene la información del juego en curso
    Game m_Game;
    //0 = normal room (chat), 1 = game room (game)
    int m_iType = 1;
    //información de la canción
    String m_sTitle, m_sDescription, m_sPassword;
    /*
     * 0: Normal Room 1: Unused 2: Room in game 3: First stage of song selection
     * has been done 4: Second stage of song selection has been done
     */
    int m_iStatus = 0;
    //Si en la sala se ha escogido una canción
    Song m_Song;
    //clients in room
    private Vector<Client> m_vClients;
    //sala padre (que contiene) a la sala actual
    private Room m_Parent = null;
    //la sala actual es el lobby?
    private boolean m_bIsLobby = false;
    
    private cl.stepnxa.persistence.Room m_bdEntity = null;

    /**
     * Crea una sala
     *
     * @param type Tipo de la sala
     * @param title Titulo de la sala
     * @param desc Descripción de la sala
     * @param pass Contraseña de la sala
     * @param lobby La sala es el lobby?
     */
    public Room(int type, String title, String desc, String pass, boolean lobby) {
        m_iType = type;
        m_sTitle = title;
        m_sDescription = desc;
        m_sPassword = pass;
        m_vClients = new Vector<Client>();
        m_Song = new Song();
        //most cases = false
        m_bIsLobby = lobby;   
                                
        m_bdEntity = new cl.stepnxa.persistence.Room();
        m_bdEntity.setName(title);            
        m_bdEntity.setDescription(desc);            
    }
    
    public cl.stepnxa.persistence.Room getRoomEntity()
    {
        return m_bdEntity;
    }

    /**
     * Agrega a la sala un cliente
     *
     * @param c Cliente a agregar
     */
    public void AddClient(Client c) {
        if (getClients() == null) {
            setClients(new Vector<Client>());
        }

        getClients().add(c);
        //System.out.println("Added client to room: " + this.m_sTitle + " named: " + c.m_sUsername);
        UpdateUserList();
        UpdateStylesAndDifficulties();

        //cuando un cliente entra a la sala y hay seleccionada una canción
        //le avisamos apenas entra que tiene que seleccionarla
        if (m_Game != null) {
            if (m_Game.getGameState() == Game.GameState.SecondSelect) {
                m_Game.AddClient(c);
                c.SelectSong(m_Song, 1);
                if (m_Song != null) {
                    c.SendServerChat("Song Selected: " + m_Song.getTitle() + " by " + m_Song.getArtist() + "\nThis song is not ranked");
                } else {
                    c.SendServerChat("Song Selected: " + m_Song.getTitle() + " by " + m_Song.getArtist() + "\nThis song is ranked");
                }
            }
        }
    }

    /**
     * Quita un cliente de la sala
     *
     * @param c Cliente de la sala
     */
    public void RemoveClient(Client c) {
        if (getClients() == null) {
            setClients(new Vector<Client>());
        }

        //quitamos el cliente altiro
        for (int i = 0; i < getClients().size(); i++) {
            if (c.m_sUsername.compareTo(getClients().get(i).m_sUsername) == 0) {
                getClients().remove(i);
                System.out.println("Client removed from room");
            }
        }

        if (!getClients().isEmpty()) {
            //System.out.println("Removing client from room: " + this.m_sTitle + " named: " + c.m_sUsername);            
            UpdateUserList();
            UpdateStylesAndDifficulties();
        } else {
            //if( this.m_iType != 0 ) //si no es lobby
            //    m_vClients.get(0).m_bRoomAdmin = true;
        }
    }

    /**
     * La sala está vacía?
     *
     * @return La sala está vacía
     */
    public boolean IsEmpty() {
        return getClients().isEmpty();
    }

    /**
     * Actualiza los clientes activos (en esta sala)
     */
    public void UpdateUserList() {
        for (int i = 0; i < getClients().size(); i++) {
            getClients().get(i).UpdateUserList(getClients());
        }
    }

    /**
     * Envia el chat a todos los clientes (de esta sala)
     *
     * @param sChatText Texto a enviar.
     * @param Owner Cliente que envió el texto
     */
    public void SendChat(String sChatText, Client Owner) {
        //GetChatColor() + GetJointNames()=WTF + " |c0ffffff: "        
        for (int i = 0; i < getClients().size(); i++) {
            getClients().get(i).SendChat(sChatText, Owner);
        }
    }

    /**
     * Informa a todos los players de la sala la canción que se seleccionó
     *
     * @param iSubCommand Indica el sub-comando de la selección (ver SMO
     * protocol)
     */
    public void SelectSong(int iSubCommand) {
        //cuando entra un cliente tarde a la sala que pasa?       
        if (m_Game != null) {
            if (m_Game.getGameState() == Game.GameState.NONE) {
                for (int i = 0; i < getClients().size(); i++) {
                    m_Game.AddClient(getClients().get(i));
                }
            }
            m_Game.StartGame(m_Song);
        } else {
            m_Game = new Game();
            SelectSong(iSubCommand);
        }
    }

    /**
     * Asigna la información de la canción seleccionada.
     *
     * @param sTitle Titulo de la canción
     * @param sArtist Artista de la canción
     * @param sHash Hash de la canción
     */
    public void SetLastSong(String sTitle, String sArtist, String sHash) {
        m_Song = Song.getRankedSong(sHash);
        if (m_Song == null) {
            m_Song = new Song();
            m_Song.setRanked(false);
            //prevenidos si la cancion no existe en la BD y nos devuelve NULL
            m_Song.setArtist(sArtist);
            m_Song.setTitle(sTitle);
        }
    }

    public void UpdateStylesAndDifficulties() {
        Vector<Client> c = (Vector<Client>) getClients().clone();
        for (Client s : c) {
            s.SendUpdateStylesAndDifficulties(c);
        }
    }

    /**
     * Devuelve el padre de la sala
     *
     * @return El padre de la sala (casi siempre el lobby)
     */
    public Room getParent() {
        return m_Parent;
    }

    /**
     * Modificamos el padre de la sala
     *
     * @param m_Parent La sala padre
     */
    public void setParent(Room m_Parent) {
        this.m_Parent = m_Parent;
    }

    /**
     * Devuelve si la sala es el lobby
     *
     * @return Retorna true si la sala es el lobby
     */
    public boolean IsLobby() {
        return m_bIsLobby;
    }

    /**
     * Asignamos si esta sala es el lobby
     *
     * @param m_bIsLobby true si la sala es el lobby
     */
    public void setIsLobby(boolean m_bIsLobby) {
        this.m_bIsLobby = m_bIsLobby;
    }

    /**
     * Obtenemos un cliente en la sala por su nombre
     *
     * @param sName El nombre del cliente.
     */
    public Client getClientByName(String sName) {
        for (Client c : getClients()) {
            if (c.m_sUsername.compareTo(sName) == 0) {
                return c;
            }
        }
        return null;
    }

    /**
     * Cambiamos el estado del player por su nombre
     *
     * @param sName El nombre del player
     * @param iState El nuevo estado del player
     */
    public void setClientState(String sName, int iState) {
        for (Client c : getClients()) {
            if (c.m_sUsername.compareTo(sName) == 0) {
                //System.out.println("Cambiando State: " + iState);
                c.m_iState = iState;
            }
        }
    }

    /**
     * @return the m_vClients
     */
    public Vector<Client> getClients() {
        return m_vClients;
    }

    /**
     * @param m_vClients the m_vClients to set
     */
    public void setClients(Vector<Client> m_vClients) {
        this.m_vClients = m_vClients;
    }
}
