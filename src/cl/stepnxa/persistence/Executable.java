/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.stepnxa.persistence;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author v1toko
 */
@Entity
@Table(name = "executable")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Executable.findAll", query = "SELECT e FROM Executable e"),
    @NamedQuery(name = "Executable.findByMd5", query = "SELECT e FROM Executable e WHERE e.md5 = :md5")})
public class Executable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "md5")
    private String md5;

    public Executable() {
    }

    public Executable(String md5) {
        this.md5 = md5;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }
    
    public static boolean testExecutableMD5(String sMd5)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("StepNXA_ServerPU", System.getProperties());
        EntityManager em = emf.createEntityManager();
        TypedQuery<Executable> q =
        em.createNamedQuery("Executable.findByMd5", Executable.class);
        //Query q = em.createNativeQuery("Player.findByUsernameAndPassword");
        q.setParameter("md5", sMd5);        
        try {
            Executable e = q.getSingleResult();
            if( e != null )
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }    
        
        return false;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (md5 != null ? md5.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Executable)) {
            return false;
        }
        Executable other = (Executable) object;
        if ((this.md5 == null && other.md5 != null) || (this.md5 != null && !this.md5.equals(other.md5))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.stepnxa.persistence.Executable[ md5=" + md5 + " ]";
    }
    
}
