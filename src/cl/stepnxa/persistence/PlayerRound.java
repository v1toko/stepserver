/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.stepnxa.persistence;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author v1toko
 */
@Entity
@Table(name = "player_round")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlayerRound.findAll", query = "SELECT p FROM PlayerRound p"),
    @NamedQuery(name = "PlayerRound.findByPlyrndId", query = "SELECT p FROM PlayerRound p WHERE p.plyrndId = :plyrndId"),
    @NamedQuery(name = "PlayerRound.findByDate", query = "SELECT p FROM PlayerRound p WHERE p.date = :date")})
public class PlayerRound implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "plyrnd_id")
    private Integer plyrndId;
    @Basic(optional = false)
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "player_id", referencedColumnName = "player_id")
    @ManyToOne(optional = false)
    private Player playerId;
    @JoinColumn(name = "round_id", referencedColumnName = "round_id")
    @ManyToOne(optional = false)
    private Round roundId;

    public PlayerRound() {
    }

    public PlayerRound(Integer plyrndId) {
        this.plyrndId = plyrndId;
    }

    public PlayerRound(Integer plyrndId, Date date) {
        this.plyrndId = plyrndId;
        this.date = date;
    }

    public Integer getPlyrndId() {
        return plyrndId;
    }

    public void setPlyrndId(Integer plyrndId) {
        this.plyrndId = plyrndId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Player getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Player playerId) {
        this.playerId = playerId;
    }

    public Round getRoundId() {
        return roundId;
    }

    public void setRoundId(Round roundId) {
        this.roundId = roundId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plyrndId != null ? plyrndId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlayerRound)) {
            return false;
        }
        PlayerRound other = (PlayerRound) object;
        if ((this.plyrndId == null && other.plyrndId != null) || (this.plyrndId != null && !this.plyrndId.equals(other.plyrndId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.stepnxa.persistence.PlayerRound[ plyrndId=" + plyrndId + " ]";
    }
    
}
