/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.stepnxa.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author v1toko
 */
@Entity
@Table(name = "song")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Song.findAll", query = "SELECT s FROM Song s"),
    @NamedQuery(name = "Song.findBySongId", query = "SELECT s FROM Song s WHERE s.songId = :songId"),
    @NamedQuery(name = "Song.findByTitle", query = "SELECT s FROM Song s WHERE s.title = :title"),
    @NamedQuery(name = "Song.findByArtist", query = "SELECT s FROM Song s WHERE s.artist = :artist"),
    @NamedQuery(name = "Song.findByMd5", query = "SELECT s FROM Song s WHERE s.md5 = :md5"),
    @NamedQuery(name = "Song.findByLink", query = "SELECT s FROM Song s WHERE s.link = :link")})
public class Song implements Serializable {    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "song_id")
    private Integer songId;
    @Basic(optional = false)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @Column(name = "artist")
    private String artist;
    @Basic(optional = true)
    @Column(name = "md5")
    private String md5;
    @Basic(optional = true)
    @Column(name = "link")
    private String link;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "songId")
    private Collection<Record> recordCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "songId")
    private Collection<Round> roundCollection;       
    @Transient
    private boolean ranked = true;

    public Song() {
    }

    public Song(Integer songId) {
        this.songId = songId;
    }
    
    public static Song getRankedSong(String sMd5)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("StepNXA_ServerPU", System.getProperties());
        EntityManager em = emf.createEntityManager();
        TypedQuery<Song> q =
        em.createNamedQuery("Song.findByMd5", Song.class);
        //Query q = em.createNativeQuery("Player.findByUsernameAndPassword");
        q.setParameter("md5", sMd5);        
        try {
            Song s = q.getSingleResult();
            return s;
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }    
    }

    public Song(Integer songId, String title, String artist, String md5, String link) {
        this.songId = songId;
        this.title = title;
        this.artist = artist;
        this.md5 = md5;
        this.link = link;
    }
    
    public void setRanked(boolean b)
    {
        ranked = b;
    }
    
    public boolean getRanked()
    {
        return ranked;
    }

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @XmlTransient
    public Collection<Record> getRecordCollection() {
        return recordCollection;
    }

    public void setRecordCollection(Collection<Record> recordCollection) {
        this.recordCollection = recordCollection;
    }

    @XmlTransient
    public Collection<Round> getRoundCollection() {
        return roundCollection;
    }

    public void setRoundCollection(Collection<Round> roundCollection) {
        this.roundCollection = roundCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (songId != null ? songId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Song)) {
            return false;
        }
        Song other = (Song) object;
        if ((this.songId == null && other.songId != null) || (this.songId != null && !this.songId.equals(other.songId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.stepnxa.persistence.Song[ songId=" + songId + " ]";
    }
    
}
