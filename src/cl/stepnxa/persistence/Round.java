/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.stepnxa.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author v1toko
 */
@Entity
@Table(name = "round")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Round.findAll", query = "SELECT r FROM Round r"),
    @NamedQuery(name = "Round.findByRoundId", query = "SELECT r FROM Round r WHERE r.roundId = :roundId"),
    @NamedQuery(name = "Round.findByPerfects", query = "SELECT r FROM Round r WHERE r.perfects = :perfects"),
    @NamedQuery(name = "Round.findByGreats", query = "SELECT r FROM Round r WHERE r.greats = :greats"),
    @NamedQuery(name = "Round.findByGoods", query = "SELECT r FROM Round r WHERE r.goods = :goods"),
    @NamedQuery(name = "Round.findByBads", query = "SELECT r FROM Round r WHERE r.bads = :bads"),
    @NamedQuery(name = "Round.findByMiss", query = "SELECT r FROM Round r WHERE r.miss = :miss"),
    @NamedQuery(name = "Round.findByScore", query = "SELECT r FROM Round r WHERE r.score = :score"),
    @NamedQuery(name = "Round.findByMeter", query = "SELECT r FROM Round r WHERE r.meter = :meter"),
    @NamedQuery(name = "Round.findByMaxcombo", query = "SELECT r FROM Round r WHERE r.maxcombo = :maxcombo")})
public class Round implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "round_id")
    private Integer roundId;
    @Basic(optional = false)
    @Column(name = "perfects")
    private int perfects;
    @Basic(optional = false)
    @Column(name = "greats")
    private int greats;
    @Basic(optional = false)
    @Column(name = "goods")
    private int goods;
    @Basic(optional = false)
    @Column(name = "bads")
    private int bads;
    @Basic(optional = false)
    @Column(name = "miss")
    private int miss;
    @Basic(optional = false)
    @Column(name = "score")
    private int score;
    @Basic(optional = false)
    @Column(name = "meter")
    private int meter;
    @Basic(optional = false)
    @Column(name = "maxcombo")
    private int maxcombo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roundId")
    private Collection<PlayerRound> playerRoundCollection;
    @JoinColumn(name = "song_id", referencedColumnName = "song_id")
    @ManyToOne(optional = false)
    private Song songId;
    @JoinColumn(name = "room_id", referencedColumnName = "room_id")
    @ManyToOne(optional = false)
    private Room roomId;
    @JoinColumn(name = "style_id", referencedColumnName = "style_id")
    @ManyToOne(optional = false)
    private Style styleId;

    public Round() {
    }

    public Round(Integer roundId) {
        this.roundId = roundId;
    }

    public Round(Integer roundId, int perfects, int greats, int goods, int bads, int miss, int score, int meter, int maxcombo) {
        this.roundId = roundId;
        this.perfects = perfects;
        this.greats = greats;
        this.goods = goods;
        this.bads = bads;
        this.miss = miss;
        this.score = score;
        this.meter = meter;
        this.maxcombo = maxcombo;
    }

    public Integer getRoundId() {
        return roundId;
    }

    public void setRoundId(Integer roundId) {
        this.roundId = roundId;
    }

    public int getPerfects() {
        return perfects;
    }

    public void setPerfects(int perfects) {
        this.perfects = perfects;
    }

    public int getGreats() {
        return greats;
    }

    public void setGreats(int greats) {
        this.greats = greats;
    }

    public int getGoods() {
        return goods;
    }

    public void setGoods(int goods) {
        this.goods = goods;
    }

    public int getBads() {
        return bads;
    }

    public void setBads(int bads) {
        this.bads = bads;
    }

    public int getMiss() {
        return miss;
    }

    public void setMiss(int miss) {
        this.miss = miss;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getMeter() {
        return meter;
    }

    public void setMeter(int meter) {
        this.meter = meter;
    }

    public int getMaxcombo() {
        return maxcombo;
    }

    public void setMaxcombo(int maxcombo) {
        this.maxcombo = maxcombo;
    }

    @XmlTransient
    public Collection<PlayerRound> getPlayerRoundCollection() {
        return playerRoundCollection;
    }

    public void setPlayerRoundCollection(Collection<PlayerRound> playerRoundCollection) {
        this.playerRoundCollection = playerRoundCollection;
    }

    public Song getSongId() {
        return songId;
    }

    public void setSongId(Song songId) {
        this.songId = songId;
    }

    public Room getRoomId() {
        return roomId;
    }

    public void setRoomId(Room roomId) {
        this.roomId = roomId;
    }

    public Style getStyleId() {
        return styleId;
    }

    public void setStyleId(Style styleId) {
        this.styleId = styleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roundId != null ? roundId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Round)) {
            return false;
        }
        Round other = (Round) object;
        if ((this.roundId == null && other.roundId != null) || (this.roundId != null && !this.roundId.equals(other.roundId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.stepnxa.persistence.Round[ roundId=" + roundId + " ]";
    }
    
}
