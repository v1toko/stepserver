/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.stepnxa.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author v1toko
 */
@Entity
@Table(name = "global_stats")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GlobalStats.findAll", query = "SELECT g FROM GlobalStats g"),
    @NamedQuery(name = "GlobalStats.findByGlobalstatsId", query = "SELECT g FROM GlobalStats g WHERE g.globalstatsId = :globalstatsId"),
    @NamedQuery(name = "GlobalStats.findByTotalPerfects", query = "SELECT g FROM GlobalStats g WHERE g.totalPerfects = :totalPerfects")})
public class GlobalStats implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "globalstats_id")
    private Integer globalstatsId;
    @Basic(optional = false)
    @Column(name = "total_perfects")
    private int totalPerfects;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "globalstatsId")
    private Collection<Player> playerCollection;

    public GlobalStats() {
    }

    public GlobalStats(Integer globalstatsId) {
        this.globalstatsId = globalstatsId;
    }

    public GlobalStats(Integer globalstatsId, int totalPerfects) {
        this.globalstatsId = globalstatsId;
        this.totalPerfects = totalPerfects;
    }

    public Integer getGlobalstatsId() {
        return globalstatsId;
    }

    public void setGlobalstatsId(Integer globalstatsId) {
        this.globalstatsId = globalstatsId;
    }

    public int getTotalPerfects() {
        return totalPerfects;
    }

    public void setTotalPerfects(int totalPerfects) {
        this.totalPerfects = totalPerfects;
    }

    @XmlTransient
    public Collection<Player> getPlayerCollection() {
        return playerCollection;
    }

    public void setPlayerCollection(Collection<Player> playerCollection) {
        this.playerCollection = playerCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (globalstatsId != null ? globalstatsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GlobalStats)) {
            return false;
        }
        GlobalStats other = (GlobalStats) object;
        if ((this.globalstatsId == null && other.globalstatsId != null) || (this.globalstatsId != null && !this.globalstatsId.equals(other.globalstatsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.stepnxa.persistence.GlobalStats[ globalstatsId=" + globalstatsId + " ]";
    }
    
}
