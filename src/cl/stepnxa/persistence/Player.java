/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.stepnxa.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author v1toko
 */
@Entity
@Table(name = "player")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Player.findAll", query = "SELECT p FROM Player p"),
    @NamedQuery(name = "Player.findByPlayerId", query = "SELECT p FROM Player p WHERE p.playerId = :playerId"),
    @NamedQuery(name = "Player.findByUsername", query = "SELECT p FROM Player p WHERE p.username = :username"),
    @NamedQuery(name = "Player.findByUsernameAndPassword", query = "SELECT p FROM Player p WHERE p.password = :password AND p.username = :username")})
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "player_id")
    private Integer playerId;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "playerId")
    private Collection<Record> recordCollection;
    @JoinColumn(name = "globalstats_id", referencedColumnName = "globalstats_id")
    @ManyToOne(optional = false)
    private GlobalStats globalstatsId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "playerId")
    private Collection<PlayerRound> playerRoundCollection;

    public static Player getLogin(String sUser, String sPass) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("StepNXA_ServerPU", System.getProperties());
        EntityManager em = emf.createEntityManager();
        TypedQuery<Player> q =
        em.createNamedQuery("Player.findByUsernameAndPassword", Player.class);        
        //Query q = em.createNativeQuery("Player.findByUsernameAndPassword");
        q.setParameter("username", sUser);
        q.setParameter("password", sPass);
        try {
            Player p = q.getSingleResult();
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }        
    }

    public Player() {
    }

    public Player(Integer playerId) {
        this.playerId = playerId;
    }

    public Player(Integer playerId, String username, String password) {
        this.playerId = playerId;
        this.username = username;
        this.password = password;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public Collection<Record> getRecordCollection() {
        return recordCollection;
    }

    public void setRecordCollection(Collection<Record> recordCollection) {
        this.recordCollection = recordCollection;
    }

    public GlobalStats getGlobalstatsId() {
        return globalstatsId;
    }

    public void setGlobalstatsId(GlobalStats globalstatsId) {
        this.globalstatsId = globalstatsId;
    }

    @XmlTransient
    public Collection<PlayerRound> getPlayerRoundCollection() {
        return playerRoundCollection;
    }

    public void setPlayerRoundCollection(Collection<PlayerRound> playerRoundCollection) {
        this.playerRoundCollection = playerRoundCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (playerId != null ? playerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Player)) {
            return false;
        }
        Player other = (Player) object;
        if ((this.playerId == null && other.playerId != null) || (this.playerId != null && !this.playerId.equals(other.playerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.stepnxa.persistence.Player[ playerId=" + playerId + " ]";
    }
}
