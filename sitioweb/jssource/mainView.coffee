require.config
	paths:
		"order":"../order.min",
		"domReady":"../domReady.min"
		"text":"../text.min"


require [
	'order!domReady'
	'order!app'
], (domReady,App) ->
	domReady () ->
		new App();