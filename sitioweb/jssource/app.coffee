define [
	'text!../templates/formLogin.html'
], (TplLogin) ->

	class Index extends Backbone.View

		events :
			"click .ingresar"	:	"login"

		el:$('.container')
		
		#Constructor 
		initialize: =>
			@render()
			true

		#Renderiza el template y pone los datos de la coleccion
		#en el template
		render: =>
			true

		login : () =>
			$('#ventana_modal').html _.template(TplLogin)
			$('#ventana_modal').modal 'show'
			$('#ventana_modal').addClass("modal-login-override")
			$('#ventana_modal').on 'hidden', () ->
				$('#ventana_modal').removeClass("modal-login-override")
