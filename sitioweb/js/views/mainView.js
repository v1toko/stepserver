(function() {

  require.config({
    paths: {
      "order": "../order.min",
      "domReady": "../domReady.min",
      "text": "../text.min"
    }
  });

  require(['order!domReady', 'order!app'], function(domReady, App) {
    return domReady(function() {
      return new App();
    });
  });

}).call(this);
