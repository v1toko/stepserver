(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  define(['text!../templates/formLogin.html'], function(TplLogin) {
    var Index;
    return Index = (function(_super) {

      __extends(Index, _super);

      function Index() {
        this.login = __bind(this.login, this);

        this.render = __bind(this.render, this);

        this.initialize = __bind(this.initialize, this);
        return Index.__super__.constructor.apply(this, arguments);
      }

      Index.prototype.events = {
        "click .ingresar": "login"
      };

      Index.prototype.el = $('.container');

      Index.prototype.initialize = function() {
        this.render();
        return true;
      };

      Index.prototype.render = function() {
        return true;
      };

      Index.prototype.login = function() {
        $('#ventana_modal').html(_.template(TplLogin));
        $('#ventana_modal').modal('show');
        $('#ventana_modal').addClass("modal-login-override");
        return $('#ventana_modal').on('hidden', function() {
          return $('#ventana_modal').removeClass("modal-login-override");
        });
      };

      return Index;

    })(Backbone.View);
  });

}).call(this);
